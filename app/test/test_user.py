from urllib.parse import urlencode
from uuid import UUID
from tornado.testing import AsyncHTTPTestCase, ExpectLog
from unittest.mock import patch

from app.web.web_server import WebAppOptions, configure_application
from app.test.util import get_async_mock_session, get_async_mock_user, get_async_none, get_async_false, get_async_true, get_async_mock_password_result_valid, get_async_mock_password_result_not_valid


class TestUserHandler(AsyncHTTPTestCase):
    def get_app(self):
        options = WebAppOptions()
        options.debug = True
        options.xsrf = False
        options.cookie_secret = "ccd70ecea6d9f0833b07688e69bf2368f86f9127de17de102e17788a805afb7f"

        return configure_application(options)

    # GET REQUESTS DOWN BELOW

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_get_default_user(self, get_current_user, get_user_by_id):
        response = self.fetch('/api/user/00000000-0000-0000-0000-000000000000')
        body = response.body.__str__()

        get_user_by_id.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'))
        self.assertRegex(body, 'b\'{"success": true, "reason": "USER RETURNED", "data": {"user": ' +
                         '{"id": "00000000-0000-0000-0000-000000000000", "name": "Test", "email": ' +
                         '"test@piratpartiet.se",')
        self.assertEqual(200, response.code)

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_no_user_id(self, get_current_user, get_user_by_id):
        with ExpectLog('Piratjakten', "Badly formatted UUID string: 1"):
            response = self.fetch('/api/user/1')
        body = response.body.__str__()

        self.assertEqual(body, 'b\'{"success": false, "reason": "USER UUID IS MISSING", "data": null}\'')
        self.assertEqual(400, response.code)

    # DELETE REQUESTS DOWN BELOW

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_no_user_id_for_delete(self, get_current_user, get_user_by_id):
        with ExpectLog('Piratjakten', "Badly formatted UUID string: 1"):
            response = self.fetch('/api/user/1', method="DELETE")
        body = response.body.__str__()

        self.assertEqual('b\'{"success": false, "reason": "USER UUID IS MISSING", "data": null}\'', body)
        self.assertEqual(400, response.code)

    # POST REQUESTS DOWN BELOW

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.database.dao.users.UsersDao.check_user_permission', return_value=get_async_false())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_no_user_id_for_post(self, get_current_user, check_user_permission, get_user_by_id):
        response = self.fetch('/api/user', method="POST", body="{}")
        body = response.body.__str__()

        self.assertEqual('b\'{"success": false, "reason": "PERMISSION DENIED", "data": null}\'', body)
        self.assertEqual(403, response.code)

    # PUT REQUESTS DOWN BELOW

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.database.dao.users.UsersDao.get_user_by_email', return_value=get_async_none())
    @patch('app.database.dao.users.UsersDao.update_user', return_value=get_async_mock_user(user_id=UUID('00000000-0000-0000-0000-000000000011'), email="new@piratpartiet.se"))
    @patch('app.database.dao.users.UsersDao.check_user_permission', return_value=get_async_false())
    @patch('app.database.dao.users.UsersDao.check_user_password', return_value=get_async_mock_password_result_not_valid())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_updating_default_email_without_permissions(self, get_current_user, check_user_password,
                                                    check_user_permission, update_user, get_user_by_email,
                                                    get_user_by_id):
        """
        Update the e-mail of a user, with id: '00000000-0000-0000-0000-000000000011', with a PUT request
        Action taken by an user with id: '00000000-0000-0000-0000-000000000000' but has no rights to update users
        This should therefore not go through with the update
        :param get_current_user: returns the user that took this action
        :param check_user_password: return a not valid PasswordResult
        :param check_user_permission: returns false
        :param update_user: returns updated user
        :param get_user_by_email: returns None to simulate no User having this new e-mail
        :param get_user_by_id: returns the user that took this action
        :return:
        """
        user_being_updated = UUID('00000000-0000-0000-0000-000000000011')
        user_taking_the_action = UUID('00000000-0000-0000-0000-000000000000')
        new_email = "new@piratpartiet.se"

        put_args = {"email": new_email}
        headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   'Content-Length': len(urlencode(put_args))}
        response = self.fetch('/api/user/00000000-0000-0000-0000-000000000011', method="PUT",
                              body=urlencode(put_args),
                              headers=headers)

        body = response.body.decode()

        check_user_permission.assert_called_with(user_taking_the_action, "user_management")
        check_user_password.assert_not_called()

        get_user_by_id.assert_called_with(user_being_updated)
        get_user_by_email.assert_not_called()

        update_user.assert_not_called()

        self.assertRegex(body, '{"success": false, "reason": "PERMISSION DENIED", "data": null}')
        self.assertEqual(403, response.code)

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.database.dao.users.UsersDao.get_user_by_email', return_value=get_async_none())
    @patch('app.database.dao.users.UsersDao.update_user', return_value=get_async_mock_user(user_id=UUID('00000000-0000-0000-0000-000000000011'), email="new@piratpartiet.se"))
    @patch('app.database.dao.users.UsersDao.check_user_permission', return_value=get_async_true())
    @patch('app.database.dao.users.UsersDao.check_user_password', return_value=get_async_mock_password_result_valid())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_updating_default_email_with_permissions(self, get_current_user, check_user_password,
                                                    check_user_permission, update_user, get_user_by_email,
                                                    get_user_by_id):
        """
        Update the e-mail of a user, with id: '00000000-0000-0000-0000-000000000011', with a PUT request
        Action taken by an user with id: '00000000-0000-0000-0000-000000000000'
        :param get_current_user: returns the user that took this action
        :param check_user_password: return a valid PasswordResult
        :param check_user_permission: returns true
        :param update_user: returns updated user
        :param get_user_by_email: returns None to simulate no User having this new e-mail
        :param get_user_by_id: returns the user that took this action
        :return:
        """
        user_being_updated = UUID('00000000-0000-0000-0000-000000000011')
        user_taking_the_action = UUID('00000000-0000-0000-0000-000000000000')
        username = "Test"
        new_email = "new@piratpartiet.se"

        put_args = {"email": new_email}
        headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   'Content-Length': len(urlencode(put_args))}
        response = self.fetch('/api/user/00000000-0000-0000-0000-000000000011', method="PUT",
                              body=urlencode(put_args),
                              headers=headers)

        body = response.body.decode()

        check_user_permission.assert_called_with(user_taking_the_action, "user_management")
        check_user_password.assert_not_called()

        get_user_by_id.assert_called_with(user_being_updated)
        get_user_by_email.assert_called_with(new_email)

        update_user.assert_called_with(user_being_updated, username, new_email)

        self.assertRegex(body, '{"success": true, "reason": "USER UPDATED, RETURNING NEW VALUES", "data": {"user": ' +
                         '{"id": "00000000-0000-0000-0000-000000000011", "name": "' + username + '", "email":' +
                         ' "' + new_email + '", "created": "')

        self.assertEqual(200, response.code)

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.database.dao.users.UsersDao.get_user_by_email', return_value=get_async_none())
    @patch('app.database.dao.users.UsersDao.update_user', return_value=get_async_mock_user(email="new@piratpartiet.se"))
    @patch('app.database.dao.users.UsersDao.check_user_permission', return_value=get_async_true())
    @patch('app.database.dao.users.UsersDao.check_user_password', return_value=get_async_mock_password_result_valid())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_updating_default_email_with_correct_password(self, get_current_user, check_user_password,
                                                    check_user_permission, update_user, get_user_by_email,
                                                    get_user_by_id):
        """
        Update the e-mail of a user, with id: '00000000-0000-0000-0000-000000000000', with a PUT request
        Action taken by the same user being updated, an user with id: '00000000-0000-0000-0000-000000000000'
        :param get_current_user: returns the user that took this action
        :param check_user_password: return a valid PasswordResult
        :param check_user_permission: returns true
        :param update_user: returns updated user
        :param get_user_by_email: returns None to simulate no User having this new e-mail
        :param get_user_by_id: returns the user that took this action
        :return:
        """
        default_uuid = UUID('00000000-0000-0000-0000-000000000000')
        username = "Test"
        old_email = "test@piratpartiet.se"
        new_email = "new@piratpartiet.se"
        correct_password = "correct"

        put_args = {"email": new_email, "password": correct_password}
        headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   'Content-Length': len(urlencode(put_args))}
        response = self.fetch('/api/user/00000000-0000-0000-0000-000000000000', method="PUT",
                              body=urlencode(put_args),
                              headers=headers)

        body = response.body.decode()

        check_user_permission.assert_called_with(default_uuid, "user_management")
        check_user_password.assert_called_with(old_email, correct_password)

        get_user_by_id.assert_called_with(default_uuid)
        get_user_by_email.assert_called_with(new_email)

        update_user.assert_called_with(default_uuid, username, new_email)

        self.assertRegex(body, '{"success": true, "reason": "USER UPDATED, RETURNING NEW VALUES", "data": {"user": ' +
                         '{"id": "00000000-0000-0000-0000-000000000000", "name": "' + username + '", "email":' +
                         ' "' + new_email + '", "created": "')

        self.assertEqual(200, response.code)

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.database.dao.users.UsersDao.get_user_by_email', return_value=get_async_none())
    @patch('app.database.dao.users.UsersDao.update_user', return_value=get_async_mock_user(email="new@piratpartiet.se"))
    @patch('app.database.dao.users.UsersDao.check_user_permission', return_value=get_async_true())
    @patch('app.database.dao.users.UsersDao.check_user_password', return_value=get_async_mock_password_result_not_valid())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_updating_default_email_with_wrong_password(self, get_current_user, check_user_password,
                                                    check_user_permission, update_user, get_user_by_email,
                                                    get_user_by_id):
        """
        Update the e-mail of a user, with id: '00000000-0000-0000-0000-000000000000', with a PUT request
        Action taken by the same user being updated, an user with id: '00000000-0000-0000-0000-000000000000'
        The wrong password is supplied and it should therefore not complete the update
        :param get_current_user: returns the user that took this action
        :param check_user_password: return a valid PasswordResult
        :param check_user_permission: returns true
        :param update_user: returns updated user
        :param get_user_by_email: returns None to simulate no User having this new e-mail
        :param get_user_by_id: returns the user that took this action
        :return:
        """
        default_uuid = UUID('00000000-0000-0000-0000-000000000000')
        old_email = "test@piratpartiet.se"
        new_email = "new@piratpartiet.se"
        wrong_password = "wrong"

        put_args = {"email": new_email, "password": wrong_password}
        headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   'Content-Length': len(urlencode(put_args))}
        response = self.fetch('/api/user/00000000-0000-0000-0000-000000000000', method="PUT",
                              body=urlencode(put_args),
                              headers=headers)

        body = response.body.decode()

        check_user_permission.assert_called_with(default_uuid, "user_management")
        check_user_password.assert_called_with(old_email, wrong_password)

        get_user_by_id.assert_called_with(default_uuid)
        get_user_by_email.assert_not_called()

        update_user.assert_not_called()

        self.assertEqual(body, '{"success": false, "reason": "UPDATE NOT ALLOWED", "data": null}')
        self.assertEqual(401, response.code)

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.database.dao.users.UsersDao.get_user_by_email', return_value=get_async_none())
    @patch('app.database.dao.users.UsersDao.update_user', return_value=get_async_mock_user(name="Pirate"))
    @patch('app.database.dao.users.UsersDao.check_user_permission', return_value=get_async_true())
    @patch('app.database.dao.users.UsersDao.check_user_password', return_value=get_async_mock_password_result_not_valid())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_updating_default_username_with_permission(self, get_current_user, check_user_password,
                                                       check_user_permission, update_user, get_user_by_email,
                                                       get_user_by_id):
        default_uuid = UUID('00000000-0000-0000-0000-000000000000')
        new_name = "Pirate"
        email = "test@piratpartiet.se"

        put_args = {"name": new_name}
        headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   'Content-Length': len(urlencode(put_args))}
        response = self.fetch('/api/user/00000000-0000-0000-0000-000000000000', method="PUT",
                              body=urlencode(put_args),
                              headers=headers)

        body = response.body.decode()

        check_user_permission.assert_called_with(default_uuid, "user_management")
        check_user_password.assert_not_called()

        get_user_by_id.assert_called_with(default_uuid)
        get_user_by_email.assert_not_called()

        update_user.assert_called_with(default_uuid, new_name, email)

        self.assertRegex(body, '{"success": true, "reason": "USER UPDATED, RETURNING NEW VALUES", "data": {"user": ' +
                         '{"id": "00000000-0000-0000-0000-000000000000", "name": "' + new_name + '", "email":' +
                         ' "' + email + '", "created": "')

        self.assertEqual(200, response.code)

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_no_user_id_for_put(self, get_current_user, get_user_by_id):
        with ExpectLog('Piratjakten', "Badly formatted UUID string: 1"):
            response = self.fetch('/api/user/1', method="PUT", body="{}")
        body = response.body.__str__()

        self.assertEqual('b\'{"success": false, "reason": "USER UUID IS MISSING", "data": null}\'', body)
        self.assertEqual(400, response.code)
