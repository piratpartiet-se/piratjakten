from app.models import Session, User, PasswordCheckResult, Bonus, Scan, Claim, Rule
from typing import Union
from hashlib import sha256
from datetime import datetime, timedelta
from uuid import UUID


async def get_async_mock_session(user_id: Union[UUID, None] = None, name: Union[str, None] = None, email: Union[str, None] = None):
    session = Session()
    session.id = UUID("00000000-0000-0000-0000-000000000000")
    session.user = User()
    session.user.id = UUID("00000000-0000-0000-0000-000000000000")
    session.user.name = "Test"
    session.user.email = "test@piratpartiet.se"
    session.user.created = datetime.now()
    session.created = datetime.now()
    session.last_used = datetime.now()
    session.hash = sha256((u"%s %s %s" % (session.id, session.user.id, session.created)).encode("utf8")).hexdigest()
    session.last_ip = "127.0.0.1"

    if user_id is not None:
        session.user.id = user_id
    if name is not None:
        session.user.name = name
    if email is not None:
        session.user.email = email

    return session


async def get_async_mock_user(user_id: Union[UUID, None] = None, name: Union[str, None] = None, email: Union[str, None] = None):
    session = await get_async_mock_session(user_id, name, email)
    user = session.user
    return user


async def get_async_none():
    return None


async def get_async_false():
    return False


async def get_async_true():
    return True


async def get_async_mock_password_result_valid(name: Union[str, None] = None, email: Union[str, None] = None) -> PasswordCheckResult:
    result = PasswordCheckResult(valid=True, user=await get_async_mock_user(name, email))
    return result


async def get_async_mock_password_result_not_valid() -> PasswordCheckResult:
    result = PasswordCheckResult(valid=False, user=None)
    return result


async def get_async_mock_claim(claim_id: Union[UUID, None] = None, code_id: Union[UUID, None] = None, user_id: Union[UUID, None] = None, longitude: Union[float, None] = None, latitude: Union[float, None] = None, points: int = 15) -> Claim:
    claim = Claim()
    claim.id = UUID("00000000-0000-0000-0000-000000000000")
    claim.user_claim_id = UUID("00000000-0000-0000-0000-000000000000")
    claim.code_id = UUID("00000000-0000-0000-0000-000000000000")
    claim.created = datetime.now()
    claim.points = points

    if claim_id is not None:
        claim.id = claim_id
    if code_id is not None:
        claim.code_id = code_id
    if user_id is not None:
        claim.user_claim_id = user_id
    if longitude is not None:
        claim.longitude = longitude
    if latitude is not None:
        claim.latitude = latitude

    return claim


async def get_async_mock_scan(scan_id: Union[UUID, None] = None, code_id: Union[UUID, None] = None,
                              user_id: Union[UUID, None] = None, longitude: Union[float, None] = None,
                              latitude: Union[float, None] = None, points: int = 10) -> Scan:
    scan = Scan()
    scan.id = UUID("00000000-0000-0000-0000-000000000000")
    scan.user_scan_id = UUID("00000000-0000-0000-0000-000000000000")
    scan.code_id = UUID("00000000-0000-0000-0000-000000000000")
    scan.created = datetime.now()
    scan.points = points

    if scan_id is not None:
        scan.id = scan_id
    if code_id is not None:
        scan.code_id = code_id
    if user_id is not None:
        scan.user_scan_id = user_id
    if longitude is not None:
        scan.longitude = longitude
    if latitude is not None:
        scan.latitude = latitude

    return scan


async def get_async_mock_rule() -> Rule:
    rule = Rule()
    rule.id = UUID("00000000-0000-0000-0000-000000000000")
    rule.name = "Standard"
    rule.claim_points = 15
    rule.scan_points = [10, 8, 6, 4, 2, 1]
    rule.bonus_points = [15, 10, 5]
    rule.non_user_points = 5
    rule.user_cooldown = timedelta(hours=22)
    rule.non_user_cooldown = timedelta(minutes=30)
    rule.created = datetime.now()

    return rule


async def get_async_int(i: int = 0):
    return i


async def get_async_timedelta_max() -> timedelta:
    return timedelta.max


async def get_async_timedelta(seconds: int = 0) -> timedelta:
    return timedelta(seconds=seconds)
