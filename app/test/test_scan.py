from urllib.parse import urlencode
from uuid import UUID
from tornado.testing import AsyncHTTPTestCase, ExpectLog
from unittest.mock import patch

from app.web.web_server import WebAppOptions, configure_application
from app.test.util import get_async_mock_session, get_async_mock_user, get_async_none, get_async_false, get_async_true, get_async_int, get_async_mock_claim, get_async_timedelta, get_async_mock_password_result_valid, get_async_mock_password_result_not_valid


class TestScanHandler(AsyncHTTPTestCase):
    def get_app(self):
        options = WebAppOptions()
        options.debug = True
        options.xsrf = False
        options.cookie_secret = "ccd70ecea6d9f0833b07688e69bf2368f86f9127de17de102e17788a805afb7f"

        return configure_application(options)

    # POST REQUESTS DOWN BELOW

    @patch('app.database.dao.users.UsersDao.get_user_by_id', return_value=get_async_mock_user())
    @patch('app.database.dao.users.UsersDao.check_user_permission', return_value=get_async_false())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_no_code_id_for_post(self, get_current_user, check_user_permission, get_user_by_id):
        response = self.fetch('/api/scan/1', method="POST", body="{}")
        body = response.body.decode()

        self.assertEqual('{"success": false, "reason": "CODE UUID IS MISSING", "data": null}', body)
        self.assertEqual(400, response.code)

    @patch('app.database.dao.rules.RulesDao._get_time_from_latest_activity', return_value=get_async_timedelta(79000))
    @patch('app.database.dao.rules.RulesDao.get_user_cooldown', return_value=get_async_timedelta(79200))
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_cooldown_active(self, get_current_user, get_user_cooldown, _get_time_from_latest_activity):
        response = self.fetch('/api/scan/00000000-0000-0000-0000-000000000000', method="POST", body="{}")
        body = response.body.decode()

        _get_time_from_latest_activity.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'),
                                                          UUID('00000000-0000-0000-0000-000000000000'))
        get_user_cooldown.assert_called_with("Standard")
        self.assertEqual('{"success": true, "reason": "COOLDOWN ACTIVE", "data": null}', body)
        self.assertEqual(200, response.code)

    @patch('app.database.dao.rules.RulesDao.is_user_cooldown_not_active', return_value=get_async_true())
    @patch('app.database.dao.rules.RulesDao.award_points_for_claim', return_value=get_async_int(15))
    @patch('app.database.dao.claims.ClaimsDao.get_claim_by_code_id', return_value=get_async_none())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_claiming_code_without_database(self, get_current_user, get_claim_by_code_id, award_points_for_claim, is_user_cooldown_not_active):
        response = self.fetch('/api/scan/00000000-0000-0000-0000-000000000000', method="POST", body="{}")
        body = response.body.decode()

        is_user_cooldown_not_active.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'),
                                                       UUID('00000000-0000-0000-0000-000000000000'))
        get_claim_by_code_id.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'))

        self.assertEqual('{"success": false, "reason": "SOMETHING WENT WRONG WHEN TRYING TO CLAIM CODE", "data": null}',
                         body)
        self.assertEqual(500, response.code)

    @patch('app.database.dao.rules.RulesDao.is_user_cooldown_not_active', return_value=get_async_true())
    @patch('app.database.dao.rules.RulesDao.award_points_for_claim', return_value=get_async_int(15))
    @patch('app.database.dao.claims.ClaimsDao.get_claim_by_code_id', return_value=get_async_none())
    @patch('app.database.dao.claims.ClaimsDao.create_claim', return_value=get_async_mock_claim())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_claiming_code(self, get_current_user, create_claim, get_claim_by_code_id, award_points_for_claim, is_user_cooldown_not_active):
        response = self.fetch('/api/scan/00000000-0000-0000-0000-000000000000', method="POST", body="{}")
        body = response.body.decode()

        is_user_cooldown_not_active.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'),
                                                       UUID('00000000-0000-0000-0000-000000000000'))

        get_claim_by_code_id.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'))

        create_claim.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'),
                                        UUID('00000000-0000-0000-0000-000000000000'), 15, None, None)

        self.assertEqual('{"success": true, "reason": "CODE CLAIMED SUCCESSFULLY", "data": null}',
                         body)
        self.assertEqual(201, response.code)

    @patch('app.database.dao.rules.RulesDao.is_user_cooldown_not_active', return_value=get_async_true())
    @patch('app.database.dao.rules.RulesDao.award_points_for_claim', return_value=get_async_int(15))
    @patch('app.database.dao.claims.ClaimsDao.get_claim_by_code_id', return_value=get_async_none())
    @patch('app.database.dao.claims.ClaimsDao.create_claim', return_value=get_async_mock_claim())
    @patch('app.web.handlers.base.BaseHandler.get_current_user', return_value=get_async_mock_session())
    def test_claiming_code(self, get_current_user, create_claim, get_claim_by_code_id, award_points_for_claim, is_user_cooldown_not_active):
        response = self.fetch('/api/scan/00000000-0000-0000-0000-000000000000', method="POST", body="{}")
        body = response.body.decode()

        is_user_cooldown_not_active.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'),
                                                       UUID('00000000-0000-0000-0000-000000000000'))

        get_claim_by_code_id.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'))

        create_claim.assert_called_with(UUID('00000000-0000-0000-0000-000000000000'),
                                        UUID('00000000-0000-0000-0000-000000000000'), 15, None, None)

        self.assertEqual('{"success": true, "reason": "CODE CLAIMED SUCCESSFULLY", "data": null}',
                         body)
        self.assertEqual(201, response.code)
