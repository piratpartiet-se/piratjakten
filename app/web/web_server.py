import asyncio
import asyncpg
import os.path
import ssl
import tornado.ioloop
import tornado.web

from configparser import ConfigParser
from tornado.platform.asyncio import AnyThreadEventLoopPolicy
from app.config import Config
from app.logger import logger
from app.web.handlers.authentication import SignInHandler, SignOutHandler, SignUpHandler
from app.web.handlers.error import Error404Handler
from app.web.handlers.main import MainHandler
from app.web.handlers.charts import ChartsHandler
from app.web.handlers.password_reset import PasswordResetHandler
from app.web.handlers.settings import SettingsHandler
from app.web.handlers.admin import AdminHandler, AdminUsersHandler, AdminGroupsHandler, AdminCategoryHandler,\
    AdminCategoriesHandler, AdminAddUserHandler, AdminAddGroupHandler, AdminAddCategoryHandler
from app.web.handlers.api.category import APICategoryHandler
from app.web.handlers.api.claims import APIClaimsHandler
from app.web.handlers.api.code import APICodeHandler
from app.web.handlers.api.codes import APICodesHandler
from app.web.handlers.api.email import APIVerifyEmailHandler
from app.web.handlers.api.user import APIUserHandler, APICreateUserHandler, APIUserGroupsHandler
from app.web.handlers.api.users import APIUsersHandler
from app.web.handlers.api.group import APIGroupHandler
from app.web.handlers.api.groups import APIGroupsHandler
from app.web.handlers.api.password import APIVerifyPasswordResetHandler
from app.web.handlers.api.scan import APIScanHandler
from app.web.handlers.api.scans import APIScansHandler
from app.web.handlers.images import QRImagesHandler
from app.web.handlers.info.privacy import InfoPrivacyHandler
from app.web.handlers.info.about import InfoAboutHandler
from app.web.handlers.map import MapHandler


class WebAppOptions:
    debug: bool = False
    https: bool = False
    port: int = 8888
    codes_filepath: os.PathLike = "codes/"
    xsrf: bool = True
    cookie_secret: str = ""
    cert_file: os.PathLike = ""
    private_file: os.PathLike = ""
    db_username: str = ""
    db_password: str = ""
    db_hostname: str = ""
    dbname: str = ""


def start():
    logger.info("Starting server...")
    ioloop = tornado.ioloop.IOLoop.current()

    config = Config.get_config()  # type: ConfigParser

    options = WebAppOptions()
    options.debug = config.getboolean("WebServer", "debug", fallback=False)
    options.https = config.getboolean("WebServer", "https", fallback=False)
    options.port = config.getint("WebServer", "port")
    options.codes_filepath = config.get("WebServer", "codes_filepath")
    options.cookie_secret = config.get("WebServer", "cookie_secret")
    options.cert_file = config.get("WebServer", "certs")
    options.private_file = config.get("WebServer", "private")
    options.db_username = config.get("PostgreSQL", "username")
    options.db_password = config.get("PostgreSQL", "password")
    options.db_hostname = config.get("PostgreSQL", "hostname")
    options.dbname = config.get("PostgreSQL", "dbname")

    configure_application(options)

    asyncio.set_event_loop_policy(AnyThreadEventLoopPolicy())
    ioloop.start()


def configure_application(options: WebAppOptions):
    ioloop = tornado.ioloop.IOLoop.current()

    webapp = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/about", InfoAboutHandler),
            (r"/api/category", APICategoryHandler),
            (r"/api/claims", APIClaimsHandler),
            (r"/api/code/(.*)", APICodeHandler),
            (r"/api/codes/(.*)", APICodesHandler),
            (r"/api/user", APICreateUserHandler),
            (r"/api/user/email/(?P<verify_id>[^\/]+)", APIVerifyEmailHandler),
            (r"/api/user/(?P<user_id>[^\/]+)", APIUserHandler),
            (r"/api/user/(?P<user_id>[^\/]+)/groups", APIUserGroupsHandler),
            (r"/api/users", APIUsersHandler),
            (r"/api/group/(.*)", APIGroupHandler),
            (r"/api/groups", APIGroupsHandler),
            (r"/api/password/(.*)", APIVerifyPasswordResetHandler),
            (r"/api/scan/(.*)", APIScanHandler),
            (r"/api/scans", APIScansHandler),
            (r"/admin", AdminHandler),
            (r"/admin/users", AdminUsersHandler),
            (r"/admin/groups", AdminGroupsHandler),
            (r"/admin/categories", AdminCategoriesHandler),
            (r"/admin/categories/(.*)", AdminCategoryHandler),
            (r"/admin/add/user", AdminAddUserHandler),
            (r"/admin/add/group", AdminAddGroupHandler),
            (r"/admin/add/category", AdminAddCategoryHandler),
            (r"/charts", ChartsHandler),
            (r"/forgot-password", PasswordResetHandler),
            (r"/images/qrcodes/(.*)", QRImagesHandler, {'path': options.codes_filepath}),
            (r"/map", MapHandler),
            (r"/privacy", InfoPrivacyHandler),
            (r"/settings", SettingsHandler),
            (r"/sign-in", SignInHandler),
            (r"/sign-out", SignOutHandler),
            (r"/sign-up", SignUpHandler),
            (r"/login", tornado.web.RedirectHandler, dict(url=r"/sign-in")),
        ],
        default_handler_class=Error404Handler,
        cookie_secret=options.cookie_secret,
        template_path=os.path.join(os.path.dirname(__file__), "..", "..", "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "..", "..", "static"),
        login_url="/sign-in",
        xsrf_cookies=options.xsrf,
        ioloop=ioloop,
        debug=options.debug
    )

    if options.https is True:
        cert_file = os.path.abspath(options.cert_file)
        private_file = os.path.abspath(options.private_file)

        if os.path.isfile(cert_file) is False:
            logger.error("Path specified in config for certification points to a non-file: " + cert_file)
            raise FileNotFoundError("Given path is not a file: " + cert_file)
        if os.path.isfile(private_file) is False:
            logger.error("Path specified in config for private key points to a non-file: " + private_file)
            raise FileNotFoundError("Given path is not a file: " + private_file)

        ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_ctx.load_cert_chain(cert_file,
                                private_file)

        http_server = tornado.httpserver.HTTPServer(webapp, ssl_options=ssl_ctx)
        http_server.listen(options.port)

        db = init_db(options)
        if db is not None:
            webapp.db = asyncio.get_event_loop().run_until_complete(db)
        else:
            webapp.db = None

        return http_server
    else:
        webapp.listen(options.port)
        db = init_db(options)
        if db is not None:
            webapp.db = asyncio.get_event_loop().run_until_complete(db)
        else:
            webapp.db = None

        return webapp


def init_db(options: WebAppOptions):
    if options.db_hostname is "" and options.dbname is "":
        return None

    dsn = "postgres://{username}:{password}@{hostname}/{database}".format(
        username=options.db_username,
        password=options.db_password,
        hostname=options.db_hostname,
        database=options.dbname
    )

    return asyncpg.create_pool(dsn, min_size=2, max_size=20, max_inactive_connection_lifetime=1800)

