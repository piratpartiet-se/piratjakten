import tornado.web

from app.logger import logger
from app.web.handlers.base import BaseHandler
from app.database.dao.users import UsersDao
from app.database.dao.rules import RulesDao


class ChartsHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        session = self.current_user

        if await dao.is_user_verified(session.user.id) is False:
            return self.render("error/notverified.html", admin=False)

        permission_check = await dao.check_user_admin(session.user.id)

        rules_dao = RulesDao(self.db)
        placements = await rules_dao.get_placements(3)  # Top 3 users
        near_placements = await rules_dao.get_placements(5, session.user.id)  # Users near the current user

        top_users = []
        for user_id in placements.keys():
            user = await dao.get_user_by_id(user_id)
            top_users.append(user)

        near_users = []
        for user_id in near_placements.keys():
            if user_id not in placements:
                placements[user_id] = near_placements[user_id]

            user = await dao.get_user_by_id(user_id)
            near_users.append(user)

        return self.render(
            "charts.html",
            admin=permission_check,
            user_id=session.user.id,
            top_users=top_users,
            near_users=near_users,
            placements=placements
        )
