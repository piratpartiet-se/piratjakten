from app.database.dao.users import UsersDao
from app.web.handlers.base import BaseHandler
from app.database.dao.email import EmailDao
from app.logger import logger
from app.email.email import send_email


class PasswordResetHandler(BaseHandler):
    def get(self):
        self.render("forgot-password.html", error="")

    async def post(self):
        email = self.get_argument("email", None)

        if email is None:
            await self.render("forgot-password.html", error="E-mail och/eller lösenord var tomt")
            return

        dao = UsersDao(self.db)
        user = await dao.get_user_by_email(email)
        if user is not None:
            email_dao = EmailDao(self.db)
            link = await email_dao.get_password_reset_link(email)

            if link == "":
                link = await email_dao.create_password_reset_link(email)
            else:
                logger.info("Password reset link already exists for e-mail: " + email)

            msg = (
                    "Någon har velat återställa lösenordet för kontot med namnet: " + user.name + "\n" +
                    "Ifall det inte var du så kan du ignorera detta mail. Annars följ länken nedanför."
            )
            logger.debug(msg)

            await send_email(email, "Piratjakten: Återställ lösenord", msg, True,
                             link)
        else:
            logger.info("User was not found when trying to create password reset link: " + email)

        await self.render("success/forgot-password.html",
                          success="Mail har skickats till: " + email + " med länk till för att återställa lösenordet")
