from app.web.handlers.base import BaseHandler
from app.database.dao.users import UsersDao


class InfoAboutHandler(BaseHandler):
    async def get(self):
        permission_check = False

        if self.is_authenticated():
            session = self.current_user
            dao = UsersDao(self.db)
            permission_check = await dao.check_user_admin(session.user.id)

        self.render("info/about.html", admin=permission_check)
