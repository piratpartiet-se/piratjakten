import os.path

from datetime import datetime, timedelta
from typing import Union

from asyncpg.pool import Pool
from tornado.web import RequestHandler, StaticFileHandler, HTTPError

from app.config import Config
from app.database.dao.users import UsersDao
from app.models import Session


class QRImagesHandler(StaticFileHandler):
    _permission = False

    @property
    def permission(self):
        return self._permission

    def data_received(self, chunk):
        # IDE seems to want to override this method, sure then
        pass

    @property
    def db(self) -> Pool:
        return self.application.db

    async def prepare(self):
        """
        Do not call manually. This runs on every request before get/post/etc.
        """
        self._current_user = await self.get_current_user()

    def is_authenticated(self) -> bool:
        """
        Check if current request is authenticated

        :returns: a boolean
        """
        return self._current_user is not None

    async def get_current_user(self) -> Union[Session, None]:
        """
        Do not use this method to get the current user session, use the property `self.current_user` instead.
        """
        session_hash = self.session_hash
        ip = self.request.remote_ip

        if session_hash is None:
            return None

        dao = UsersDao(self.db)
        session = await dao.get_session_by_hash(session_hash)

        if session is None:
            # Session hash is not in the database
            self.clear_session_cookie()
            return None

        max_idle_days = Config.get_config().getint("Session", "max_idle_days")

        # Check if session is older than max configured idle days, if it is; sign out
        if session.last_used < datetime.now() - timedelta(days=max_idle_days):
            await dao.remove_session(session_hash)
            self.clear_session_cookie()
            return None

        new_session = await dao.update_session(session_hash, ip)

        permission_check = await dao.check_user_permission(new_session.user.id, 'category_management')

        self._permission = permission_check

        return new_session

    @property
    def current_user(self) -> Union[Session, None]:
        """
        Get the current user session object that includes a object for the current user.
        :return: A Session object if authenticated, otherwise None
        """
        return super().current_user

    @property
    def session_hash(self) -> Union[str, None]:
        session_hash = self.get_secure_cookie("session")
        return session_hash.decode("utf8") if session_hash is not None else None

    def set_session_cookie(self, value=None):
        if value is not None and value != "":
            self.set_secure_cookie("session", value)

    def clear_session_cookie(self):
        self.clear_cookie("session")

    def respond(self, message:str, status_code:int=200, json_data=None):
        self.set_status(status_code, message)

        if status_code >= 400:
            self.write({'success': False, 'reason': message, 'data': json_data})
        else:
            self.write({'success': True, 'reason': message, 'data': json_data})

    def validate_absolute_path(self, root, absolute_path):
        if self.permission is False:
            raise HTTPError(403, "User does not have permission to view categories")

        root = os.path.abspath(root)

        if not root.endswith(os.path.sep):
            root += os.path.sep

        if not (absolute_path + os.path.sep).startswith(root):
            raise HTTPError(403, "%s is not in root static directory",
                            self.path)
        if (os.path.isdir(absolute_path) and
                self.default_filename is not None):

            if not self.request.path.endswith("/"):
                self.redirect(self.request.path + "/", permanent=True)
                return

            absolute_path = os.path.join(absolute_path, self.default_filename)

        if not os.path.exists(absolute_path):
            raise HTTPError(404)
        if not os.path.isfile(absolute_path):
            raise HTTPError(403, "%s is not a file", self.path)
        return absolute_path

    def write_error(self, status_code, **kwargs):
        if status_code == 403:
            return self.render("error/403.html", admin=False)

        return self.write("Någonting gick fel: " + status_code.__str__())
