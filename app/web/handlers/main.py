import tornado.web

from pytz import timezone
from app.logger import logger
from app.models import Scan, Claim, Bonus
from app.web.handlers.base import BaseHandler
from app.database.dao.users import UsersDao
from app.database.dao.scans import ScansDao
from app.database.dao.claims import ClaimsDao
from app.database.dao.bonuses import BonusesDao
from app.database.dao.rules import RulesDao, get_points_to_next_level
from app.database.dao.categories import CategoriesDao


class MainHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        session = self.current_user
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_admin(session.user.id)

        scan_dao = ScansDao(self.db)
        claim_dao = ClaimsDao(self.db)
        bonus_dao = BonusesDao(self.db)
        category_dao = CategoriesDao(self.db)
        rules_dao = RulesDao(self.db)

        placements = await rules_dao.get_placements(1, session.user.id)
        placement, points, level = placements[session.user.id]

        show_by_default = 10
        activities = []
        scans = await scan_dao.get_scans_by_user_id(session.user.id)
        claims = await claim_dao.get_claims_by_user_id(session.user.id)
        bonuses = await bonus_dao.get_bonuses_by_user_id(session.user.id, 10)

        scan_count = len(scans) + len(claims)

        activities.extend(scans)
        activities.extend(bonuses)
        activities.extend(claims)

        def get_created_timestamp(s):
            return s.created

        activities.sort(reverse=True, key=get_created_timestamp)
        activities = activities[0:show_by_default]

        names = []
        for activity in activities:
            activity.created = activity.created.replace(tzinfo=timezone('UTC'))
            activity.created = activity.created.astimezone(timezone('Europe/Stockholm'))

            if hasattr(activity, "code_id"):
                category = await category_dao.get_category_by_code_id(activity.code_id)
                names.append(category.name)
            elif hasattr(activity, "scan_id"):
                scan = await scan_dao.get_scan_by_id(activity.scan_id)
                category = await category_dao.get_category_by_code_id(scan.code_id)
                names.append(category.name)

        extra_title = ""
        if scan_count > show_by_default:
            extra_title = "Visar " + show_by_default.__str__() + " senaste aktiviteterna"
        elif scan_count is 0:
            extra_title = "Inga aktiviteter finns att rapportera"

        await self.render("main.html",
                          user=session.user, points=points,
                          next_level=get_points_to_next_level(points),
                          admin=permission_check,
                          scans=activities, names=names,
                          scan_count=scan_count,
                          extra_title=extra_title,
                          level=level,
                          placement=placement,
                          Scan=Scan,
                          Claim=Claim)
