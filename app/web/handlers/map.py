import tornado.web

from app.web.handlers.base import BaseHandler
from app.database.dao.users import UsersDao


class MapHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        session = self.current_user
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_admin(session.user.id)

        self.render("map.html", user_id=session.user.id, email=session.user.email, username=session.user.name, admin=permission_check)
