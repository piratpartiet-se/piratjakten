import tornado.web


from uuid import UUID
from app.database.dao.users import UsersDao
from app.database.dao.groups import GroupsDao
from app.database.dao.categories import CategoriesDao
from app.web.handlers.base import BaseHandler


class AdminHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        user_dao = UsersDao(self.db)
        permission_check = await user_dao.check_user_admin(self.current_user.user.id)

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        user_count = await user_dao.get_user_count()
        verified_count = await user_dao.get_verified_user_count()

        group_dao = GroupsDao(self.db)
        group_count = await group_dao.get_group_count()

        self.render("admin/summary.html", admin=True, view=0, user_count=user_count, verified_count=verified_count,
                    group_count=group_count)


class AdminUsersHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "user_management")

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        user_count = await dao.get_user_count()

        group_dao = GroupsDao(self.db)
        groups = await group_dao.get_all_groups()

        self.render("admin/users.html", admin=True, view=1, user_count=user_count, groups=groups)

class AdminGroupsHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "group_management")

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        group_dao = GroupsDao(self.db)
        group_count = await group_dao.get_group_count()

        self.render("admin/groups.html", admin=True, view=2, group_count=group_count)

class AdminCategoriesHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "category_management")

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        dao = CategoriesDao(self.db)
        categories = await dao.get_categories()

        self.render("admin/categories.html", admin=True, view=3, categories=categories)


class AdminCategoryHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self, id:UUID):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "category_management")

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        dao = CategoriesDao(self.db)
        category = await dao.get_category_by_id(id)

        self.render("admin/category_handler.html", admin=True, category=category)


class AdminAddUserHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "user_management")

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        self.render("admin/add_user.html", admin=True,)


class AdminAddGroupHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "group_management")

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        dao = GroupsDao(self.db)
        permissions = await dao.get_permissions()

        self.render("admin/add_group.html", admin=True, permissions=permissions)

class AdminAddCategoryHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "category_management")

        if permission_check is False:
            self.set_status(403, "PERMISSION DENIED")
            return self.write("PERMISSION DENIED")

        self.render("admin/add_category.html", admin=True,)
