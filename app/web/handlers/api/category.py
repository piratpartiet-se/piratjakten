import tornado.web

from uuid import UUID
from app.models import category_to_json
from app.database.dao.users import UsersDao
from app.database.dao.groups import GroupsDao
from app.database.dao.categories import CategoriesDao
from app.web.handlers.base import BaseHandler


class APICategoryHandler(BaseHandler):
    async def get(self, category_id: UUID):
        if self.is_authenticated():
            dao = UsersDao(self.db)
            permission_check = await dao.check_user_permission(self.current_user.user.id, "category_management")

            if permission_check is False:
                return self.respond("PERMISSION DENIED", 403)

            dao = CategoriesDao(self.db)
            category = await dao.get_category_by_id(category_id)

            return self.respond("RETURNED CATEGORY", 201, category_to_json(category))

    @tornado.web.authenticated
    async def post(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "category_management")

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        name = self.get_argument("name", None)
        description = self.get_argument("description", None)
        info_url = self.get_argument("infoUrl", None)

        if name is None:
            return self.respond("NAME IS MISSING", 400)
        elif description is None:
            description = ""
        elif info_url is None:
            info_url = "https://www.piratpartiet.se/"

        dao = CategoriesDao(self.db)
        category = await dao.create_category(name, description, info_url)

        return self.respond("CREATED CATEGORY", 201, category_to_json(category))
