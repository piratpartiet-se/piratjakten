import tornado.web

from uuid import UUID

from app.logger import logger
from app.models import group_to_json
from app.database.dao.users import UsersDao
from app.database.dao.groups import GroupsDao
from app.web.handlers.base import BaseHandler


class APIGroupHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self, group_id: UUID):
        group_id = self.check_uuid(group_id)
        if group_id is None:
            return self.respond("GROUP UUID IS MISSING", 400)

        group_dao = GroupsDao(self.db)
        group = await group_dao.get_group_by_id(group_id)

        if group is None:
            return self.respond("GROUP COULD NOT BE FOUND", 404)

        return self.respond("GROUP RETURNED", 200, group_to_json(group))

    @tornado.web.authenticated
    async def post(self, query=None):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "group_management")

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        name = self.get_argument("name", None)
        level = self.get_argument("level", None)

        if name is None:
            return self.respond("NAME IS MISSING", 400)
        elif level is None:
            return self.respond("LEVEL IS MISSING", 400)

        dao = GroupsDao(self.db)
        group = await dao.get_group_by_name(name)

        permissions = await dao.get_permissions()
        permissions_values = {}

        for p in permissions:
            value = self.get_argument(p, None)
            if value == "true":
                permissions_values[p] = True

        if group is None:
            level = int(level)
            group = await dao.create_group(name, level, permissions_values)
        else:
            return self.respond("GROUP WITH THAT NAME ALREADY EXIST", 409)

        return self.respond("CREATED GROUP", 201, group_to_json(group))

    @tornado.web.authenticated
    async def put(self, group_id: UUID):
        group_id = self.check_uuid(group_id)
        if group_id is None:
            return self.respond("GROUP UUID IS MISSING", 400)

        user_dao = UsersDao(self.db)
        permission_check = await user_dao.check_user_permission(self.current_user.user.id, "group_management")

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        group_dao = GroupsDao(self.db)
        group = await group_dao.get_group_by_id(group_id)

        name = self.get_argument("name", None)
        level = self.get_argument("level", None)

        try:
            if level is not None:
                level = int(level)
        except ValueError as exc:
            logger.debug(exc)
            logger.warning("Value provided for 'level', when trying to update group: " + group_id.__str__() +
                           ", was not an integer")
            return self.respond("LEVEL COULD NOT BE PARSED AS AN INTEGER", 400)

        permissions = await group_dao.get_permissions()
        permissions_values = {}

        for p in permissions:
            value = self.get_argument(p, None)
            if value == "true":
                permissions_values[p] = True
            elif value == "false":
                permissions_values[p] = False

        if (name is None or name == group.name) and (level is None or level == group.level) and\
                len(permissions_values) is 0:
            return self.respond("NO CHANGE", 200)

        if name is None:
            logger.debug("'name' was None, setting it to group.name: " + group.name)
            name = group.name
        if level is None:
            logger.debug("'level' was None, setting it to group.level: " + group.level.__str__())
            level = group.level

        logger.info("Updating group: " + group_id.__str__() + ", with name: " + name + " and level: " + level.__str__())

        for p in permissions_values.keys():
            logger.info("Group permission " + p + ": " + permissions_values[p].__str__())

        logger.info("Action taken by user: " + self.current_user.user.id.__str__())

        await group_dao.update_group(group_id, name, level)
        await group_dao.set_permissions_for_group(group_id, permissions_values)
        group = await group_dao.get_group_by_id(group.id)

        return self.respond("GROUP UPDATED, RETURNING NEW VALUES", 200, group_to_json(group))

    @tornado.web.authenticated
    async def delete(self, group_id: UUID):
        group_id = self.check_uuid(group_id)
        if group_id is None:
            return self.respond("GROUP UUID IS MISSING", 400)

        user_dao = UsersDao(self.db)
        permission_check = await user_dao.check_user_permission(self.current_user.user.id, 'delete_groups')

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        logger.info("Group: " + group_id.__str__() + " is being deleted by user: " +
                    self.current_user.user.id.__str__())
        group_dao = GroupsDao(self.db)
        await group_dao.delete_group(group_id)

        return self.respond("SUCCESSFULLY DELETED GROUP(S)")
