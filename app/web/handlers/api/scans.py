from app.logger import logger
from app.database.dao.scans import ScansDao
from app.models import scan_to_json
from app.web.handlers.base import BaseHandler


class APIScansHandler(BaseHandler):
    async def get(self):
        amount = self.get_argument("amount", None)

        if amount is None:
            return self.respond("AMOUNT WAS NOT PROVIDED", 400)
        else:
            try:
                amount = int(amount)
            except ValueError as exc:
                logger.debug(exc)
                logger.error("Amount was not a number")
                return self.respond("AMOUNT WAS NOT A NUMBER", 400)

        if amount > 1000:
            logger.info("Amount was larger than 1000, limiting it to 1000")
            amount = 1000

        scans_dao = ScansDao(self.db)
        scans = await scans_dao.get_scans(amount)

        scan_list = []
        for scan in scans:
            scan.id = None
            scan.user_scan_id = None
            scan.code_id = None
            scan_list.append(scan_to_json(scan))

        return self.respond("SCAN(S) RETURNED", 200, scan_list)
