from app.logger import logger
from app.database.dao.claims import ClaimsDao
from app.models import claim_to_json
from app.web.handlers.base import BaseHandler


class APIClaimsHandler(BaseHandler):
    async def get(self):
        amount = self.get_argument("amount", None)

        if amount is None:
            return self.respond("AMOUNT WAS NOT PROVIDED", 400)
        else:
            try:
                amount = int(amount)
            except ValueError as exc:
                logger.debug(exc)
                logger.error("Amount was not a number")
                return self.respond("AMOUNT WAS NOT A NUMBER", 400)

        if amount > 1000:
            logger.info("Amount was larger than 1000, limiting it to 1000")
            amount = 1000

        claims_dao = ClaimsDao(self.db)
        claims = await claims_dao.get_claims(amount)
        claim_list = []

        for claim in claims:
            claim.id = None
            claim.user_claim_id = None
            claim.code_id = None
            claim_list.append(claim_to_json(claim))

        if len(claim_list) is 0:
            claim_list = None

        return self.respond("CLAIM(S) RETURNED", 200, claim_list)
