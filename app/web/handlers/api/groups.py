import tornado.web

from uuid import UUID
from app.database.dao.users import UsersDao
from app.database.dao.groups import GroupsDao
from app.web.handlers.base import BaseHandler


class APIGroupsHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check1 = await dao.check_user_permission(self.current_user.user.id, 'group_management')
        permission_check2 = await dao.check_user_permission(self.current_user.user.id, 'delete_groups')

        if permission_check1 is False and permission_check2 is False:
            return self.respond("PERMISSION DENIED", 403)

        draw = self.get_argument("draw")
        start = self.get_argument("start")
        length = self.get_argument("length")
        global_search = self.get_argument("search[value]")
        order_column = self.get_argument("order[0][column]")
        order_dir = self.get_argument("order[0][dir]")

        order_column = self.get_argument("columns[" + order_column + "][data]")

        draw = int(draw)
        start = int(start)
        length = int(length)

        dao = GroupsDao(self.db)

        if length > 0:
            group_list = await dao.get_groups(global_search, start, length, order_column, order_dir)
        else:
            return self.respond("LENGTH IS SMALLER THAN 0", 400)

        records_total = await dao.get_group_count()
        if global_search is None or global_search == "":
            records_filtered = records_total
        else:
            records_filtered = await dao.get_group_count(global_search)

        response = {
            'success': True,
            'draw': draw,
            'recordsTotal': records_total,
            'recordsFiltered': records_filtered,
            'data': group_list
        }

        return self.write(response)

    @tornado.web.authenticated
    async def delete(self):
        user_dao = UsersDao(self.db)
        permission_check = await user_dao.check_user_permission(self.current_user.user.id, 'delete_groups')

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        uuids = self.get_arguments("uuid[]")

        group_dao = GroupsDao(self.db)

        for group_id in uuids:
            await group_dao.delete_group(group_id)

        return self.respond("SUCCESSFULLY DELETED GROUP(S)")