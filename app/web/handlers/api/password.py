import string
import random

from uuid import UUID
from app.email.email import send_email
from app.web.handlers.base import BaseHandler
from app.database.dao.users import UsersDao
from app.database.dao.email import EmailDao
from app.logger import logger


class APIVerifyPasswordResetHandler(BaseHandler):
    async def get(self, verify_id: UUID):
        verify_id = self.check_uuid(verify_id)
        if verify_id is None:
            return self.respond("VERIFY UUID IS MISSING", 400, show_error_page=True)

        user_dao = UsersDao(self.db)
        email_dao = EmailDao(self.db)

        email = await email_dao.get_email_by_password_reset_link(verify_id)
        await email_dao.remove_password_reset_link_for_email(email)
        new_password = random_generator(size=10)

        user = await user_dao.get_user_by_email(email)
        if user is None:
            logger.warning("Could not find user with e-mail: " + email)
            return self.respond("VERIFY UUID WAS NOT ASSOCIATED WITH ANY E-MAIL", 404, show_error_page=True)
        else:
            await user_dao.update_user(user.id, user.name, user.email, new_password)
            await send_email(email, "Piratjakten: Lösenord återställt",
                             "Ditt lösenord för Piratjakten är nu återställt till: " + new_password)

        await self.render("success/password_reset.html", admin=False)


def random_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))
