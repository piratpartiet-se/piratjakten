import tornado.web

from uuid import UUID
from app.config import Config
from app.logger import logger
from app.database.dao.email import EmailDao
from app.database.dao.groups import GroupsDao
from app.database.dao.users import UsersDao
from app.web.handlers.base import BaseHandler
from app.models import user_to_json


class APIUserHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self, user_id: UUID = None):
        user_id = self.check_uuid(user_id)
        if user_id is None:
            return self.respond("USER UUID IS MISSING", 400)

        dao = UsersDao(self.db)
        user = await dao.get_user_by_id(user_id)

        if user is None:
            return self.respond("USER COULD NOT BE FOUND", 404)

        return self.respond("USER RETURNED", 200, user_to_json(user))

    @tornado.web.authenticated
    async def put(self, user_id: UUID):
        user_id = self.check_uuid(user_id)
        if user_id is None:
            return self.respond("USER UUID IS MISSING", 400)

        dao = UsersDao(self.db)

        user = await dao.get_user_by_id(user_id)
        if user is None:
            return self.respond("USER COULD NOT BE FOUND", 404)

        user_permission = await dao.check_user_permission(self.current_user.user.id, "user_management")

        if user_id != self.current_user.user.id and user_permission is False:
            return self.respond("PERMISSION DENIED", 403)

        email = self.get_argument("email", None)
        name = self.get_argument("name", None)
        password = self.get_argument("password", None)
        password1 = self.get_argument("password1", None)
        password2 = self.get_argument("password2", None)
        verified_email = self.get_argument("verified_email", None)
        verified = self.get_argument("verified", None)
        groups = self.get_arguments("groups[]")

        if verified_email == "true":
            verified_email = True
        elif verified_email == "false":
            verified_email = False

        if verified == "true":
            logger.debug("'verified' is set to True")
            verified = True
        elif verified == "false":
            logger.debug("'verified' is set to False")
            verified = False

        password_result = False
        if password is not None:
            password_result = await dao.check_user_password(user.email, password)
            password_result = password_result.valid

        logger.debug("User updating itself: " + (user_id == self.current_user.user.id).__str__())
        logger.debug("User permission: " + user_permission.__str__())
        logger.debug("Password result: " + password_result.__str__())

        config = Config.get_config()
        max_email_length = config.getint("Users", "max_email_length")
        max_name_length = config.getint("Users", "max_username_length")
        max_password_length = config.getint("Users", "max_password_length")
        min_password_length = config.getint("Users", "min_password_length")

        if name is None:
            name = user.name
        elif len(name) > max_name_length:
            return self.respond("NAME CAN'T BE LONGER THAN " + max_name_length.__str__() + " CHARACTERS", 400)

        if email is None:
            email = user.email
        elif len(email) > max_email_length:
            return self.respond("E-MAIL CAN'T BE LONGER THAN " + max_email_length.__str__() + " CHARACTERS", 400)
        else:
            if password_result is not True and (user_permission is False or user_id == self.current_user.user.id):
                return self.respond("UPDATE NOT ALLOWED", 401)

            if email == user.email:
                logger.debug("E-mail is the same as the one currently used, ignoring")
            else:
                check_user = await dao.get_user_by_email(email)

                if check_user is not None:
                    return self.respond("NEW E-MAIL IS ALREADY IN USE", 409)

        if verified is not None:
            permission_check = await dao.check_user_permission(self.current_user.user.id, "verify")

            if permission_check is False:
                return self.respond("PERMISSION DENIED", 403)
            elif verified is True:
                logger.debug("Verifying user: " + user_id.__str__())
                await dao.verify_user_by_id(user_id)
            elif verified is False:
                logger.debug("Unverifying user: " + user_id.__str__())
                await dao.unverify_user_by_id(user_id)

        if len(groups) > 0:
            group_permission = await dao.check_user_permission(self.current_user.user.id, "group_management")

            if group_permission is False:
                return self.respond("PERMISSION DENIED", 403)

            if groups[0] == "None":
                groups = []

            groups_dao = GroupsDao(self.db)
            await groups_dao.set_users_groups(user_id, groups)

        if password1 is not None:
            if len(password1) > max_password_length:
                max_str = max_password_length.__str__()
                return self.respond("PASSWORD CAN'T BE LONGER THAN " + max_str + " CHARACTERS", 400)
            elif len(password1) < min_password_length:
                min_str = min_password_length.__str__()
                return self.respond("PASSWORD CAN'T BE SHORTER THAN " + min_str + " CHARACTERS", 400)
            elif password1 != password2:
                return self.respond("PASSWORDS DO NOT MATCH", 400)
            elif password_result is not True and (user_permission is False or user_id == self.current_user.user.id):
                return self.respond("UPDATE NOT ALLOWED", 401)

            logger.debug("Updating, with password, user: " + user_id.__str__())
            user = await dao.update_user(user_id, name, email, password1)
        else:
            logger.debug("Updating, without password, user: " + user_id.__str__())
            user = await dao.update_user(user_id, name, email)

        if user is None:
            return self.respond("SOMETHING WENT WRONG WHEN UPDATING USER", 500)

        if verified_email is True:
            email_dao = EmailDao(self.db)
            await email_dao.verify_email(email)
        elif verified_email is False:
            email_dao = EmailDao(self.db)
            await email_dao.unverify_email(email)

        return self.respond("USER UPDATED, RETURNING NEW VALUES", 200, user_to_json(user))
    
    @tornado.web.authenticated
    async def delete(self, user_id: UUID = None):
        user_id = self.check_uuid(user_id)
        if user_id is None:
            return self.respond("USER UUID IS MISSING", 400)

        dao = UsersDao(self.db)

        if user_id != self.current_user.user.id:
            permission_check = await dao.check_user_permission(self.current_user.user.id, "delete_users")

            if permission_check is False:
                self.respond("PERMISSION DENIED", 403)

        user = await dao.get_user_by_id(user_id)

        if user is None:
            return self.respond("USER COULD NOT BE FOUND", 404)

        await dao.remove_user(user.id)

        return self.respond("SUCCESSFULLY DELETED USER")


class APICreateUserHandler(BaseHandler):
    @tornado.web.authenticated
    async def post(self, user_id: UUID = None):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "user_management")

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        email = self.get_argument("email", None)
        name = self.get_argument("name", None)
        password = self.get_argument("password", None)
        password2 = self.get_argument("password2", None)
        verified_email = self.get_argument("verified_email", None)
        verified = self.get_argument("verified", None)

        if verified_email == "true":
            verified_email = True
        else:
            verified_email = False
        if verified == "true":
            verified = True
        else:
            verified = False

        if email is None:
            return self.respond("E-MAIL IS MISSING", 400)
        elif name is None:
            return self.respond("NAME IS MISSING", 400)
        elif password is None or password2 is None:
            return self.respond("PASSWORD(S) IS MISSING", 400)
        elif password != password2:
            return self.respond("PASSWORDS DO NOT MATCH", 400)

        config = Config.get_config()
        max_email_length = config.getint("Users", "max_email_length")
        max_name_length = config.getint("Users", "max_username_length")
        max_password_length = config.getint("Users", "max_password_length")
        min_password_length = config.getint("Users", "min_password_length")

        if len(email) > max_email_length:
            max_str = max_email_length.__str__()
            return self.respond("E-MAIL CAN'T BE LONGER THAN " + max_str + " CHARACTERS", 400)
        elif len(name) > max_name_length:
            max_str = max_name_length.__str__()
            return self.respond("NAME CAN'T BE LONGER THAN " + max_str + " CHARACTERS", 400)
        elif len(password) > max_password_length:
            max_str = max_password_length.__str__()
            return self.respond("PASSWORD CAN'T BE LONGER THAN " + max_str + " CHARACTERS", 400)
        elif len(password) < min_password_length:
            min_str = min_password_length.__str__()
            return self.respond("PASSWORD CAN'T BE SHORTER THAN " + min_str + " CHARACTERS", 400)

        user = await dao.get_user_by_email(email)

        if user is None:
            user = await dao.create_user(name, email, password)
        else:
            return self.respond("USER WITH THAT E-MAIL ALREADY EXIST", 409)

        if verified is True:
            dao.verify_user_by_id(user.id)
        if verified_email is True:
            email_dao = EmailDao(self.db)
            await email_dao.verify_email(user.email)

        return self.respond("CREATED USER", 201, user_to_json(user))


class APIUserGroupsHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self, user_id: UUID = None):
        user_id = self.check_uuid(user_id)
        if user_id is None:
            return self.respond("USER UUID IS MISSING", 400)

        if self.current_user.user.id == user_id:
            dao = GroupsDao(self.db)
            groups = await dao.get_groups_by_user_id(user_id)

            return self.respond("GROUPS RETURNED", 200, groups)
        else:
            dao = UsersDao(self.db)
            permission_check = await dao.check_user_permission(self.current_user.user.id, 'group_management')

            if permission_check is True:
                dao = GroupsDao(self.db)
                groups = await dao.get_groups_by_user_id(user_id)

                names = []
                for group in groups:
                    names.append(group.name)

                return self.respond("GROUPS RETURNED", 200, names)
            else:
                return self.respond("PERMISSION DENIED", 403)