from app.logger import logger
import tornado.web

from uuid import UUID
from app.models import code_to_json

from app.web.handlers.base import BaseHandler

from app.database.dao.users import UsersDao
from app.database.dao.bonuses import BonusesDao
from app.database.dao.claims import ClaimsDao
from app.database.dao.codes import CodesDao
from app.database.dao.rules import RulesDao
from app.database.dao.scans import ScansDao


class APICodeHandler(BaseHandler):
    async def get(self, code_id: UUID):
        code_id = self.check_uuid(code_id)
        if code_id is None:
            return self.respond("CODE UUID IS MISSING", 400, show_error_page=True)

        rules_dao = RulesDao(self.db)

        if self.is_authenticated():
            user_id = self.current_user.user.id

            result = await rules_dao.is_user_cooldown_not_active(code_id, user_id)
            if result is True:
                return self.render("code.html", authenticated=True, redirect_url="/", code_id=code_id)
            else:
                logger.debug("User: " + user_id.__str__() + ", tried to scan code: " + code_id.__str__() +
                              "but cooldown is active.")
                return self.render("code.html", authenticated=False, redirect_url="/", code_id=code_id)
        else:
            codes_dao = CodesDao(self.db)
            scans_dao = ScansDao(self.db)

            code = await codes_dao.get_code_by_id(code_id)
            logger.info("Code: " + code_id.__str__() + " was scanned by a non user")

            if code is None:
                self.set_status(404, "CODE COULD NOT BE FOUND")
                return self.render("code.html", authenticated=False, redirect_url="/", code_id=code_id)

            result = await rules_dao.is_non_user_cooldown_not_active(code_id)
            if result is True:
                logger.debug("Non-user cooldown was not active for code: " + code_id.__str__())

                scan = await scans_dao.create_scan(None, code_id, 0)
                logger.debug("Scan created for non-user: " + scan.id.__str__())

                claim_dao = ClaimsDao(self.db)
                claim = await claim_dao.get_claim_by_code_id(code_id)

                if claim is not None:
                    logger.debug("Claim was found for code: " + code_id.__str__() + "\n" +
                                 "Claim ID: " + claim.id.__str__())

                    bonuses_dao = BonusesDao(self.db)
                    points = await rules_dao.award_points_for_non_user_bonus(code_id, claim.user_claim_id)
                    await bonuses_dao.create_bonus(claim.user_claim_id, scan.id, points)

                    logger.info("Bonus created for user: " + claim.user_claim_id.__str__())
            else:
                logger.debug("Non-user cooldown was active for code: " + code_id.__str__())

            return self.render("code.html", authenticated=False, redirect_url=code.category.info_url, code_id=code_id)

    @tornado.web.authenticated
    async def post(self, category_id: UUID):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "category_management")

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        amount = self.get_argument("no", default="1")

        try:
            amount = int(amount)
        except ValueError as exc:
            logger.debug(exc)
            amount = 1

        amount = min(max(1, amount), 1000)  # Don't allow more than 1k codes.
        logger.info('Number of codes to be generated: ', amount)

        dao = CodesDao(self.db)
        codes = await dao.create_codes(category_id, amount)

        return self.respond("CREATED CODES", 201, {'codes' : [code_to_json(code) for code in codes]})
