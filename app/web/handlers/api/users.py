import tornado.web

from app.logger import logger
from app.database.dao.users import UsersDao
from app.database.dao.groups import GroupsDao
from app.web.handlers.base import BaseHandler


class APIUsersHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "user_management")

        if permission_check is False:
            self.respond("PERMISSION DENIED", 403)

        draw = self.get_argument("draw")
        start = self.get_argument("start")
        length = self.get_argument("length")
        global_search = self.get_argument("search[value]")
        order_column = self.get_argument("order[0][column]")
        order_dir = self.get_argument("order[0][dir]")

        order_column = self.get_argument("columns[" + order_column + "][data]")

        if order_column == "verified_email":
            order_column = "email"

        draw = int(draw)
        start = int(start)
        length = int(length)

        if length > 0:
            user_list = await dao.get_users(global_search, order_column, order_dir)
        else:
            return self.respond("LENGTH IS SMALLER THAN 0", 400)

        length += start

        records_filtered = len(user_list)
        user_list = user_list[start:length]

        response = {
            'success': True,
            'draw': draw,
            'recordsTotal': await dao.get_user_count(),
            'recordsFiltered': records_filtered,
            'data': user_list
        }

        return self.write(response)

    @tornado.web.authenticated
    async def put(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "verify")

        if permission_check is False:
            self.respond("PERMISSION DENIED", 403)

        permission_check = await dao.check_user_permission(self.current_user.user.id, "group_management")

        emails = self.get_arguments("emails[]")
        verify = self.get_arguments("verify[]")
        groups = self.get_arguments("groups[]")

        logger.debug("Length of emails: " + len(emails).__str__())
        logger.debug("Length of verify: " + len(verify).__str__())
        logger.debug("Length of groups: " + len(groups).__str__())

        for index, email in enumerate(emails):
            user = await dao.get_user_by_email(email)
            if user is not None:
                if index < len(verify):
                    if verify[index] == "true":
                        logger.info("Verifying user with e-mail: " + email + "\n" +
                                    "Action taken by: " + self.current_user.user.id.__str__())
                        if await dao.verify_user_by_email(email) is False:
                            return self.respond("COULD NOT VERIFY USER(S) FOR UNKNOWN REASON", 500)
                    elif verify[index] == "false":
                        logger.info("Unverifying user with e-mail: " + email + "\n" +
                                    "Action taken by: " + self.current_user.user.id.__str__())
                        if await dao.unverify_user_by_email(email) is False:
                            return self.respond("COULD NOT UNVERIFY USER(S) FOR UNKNOWN REASON", 500)
                    else:
                        logger.warning("Bad value for verify[" + index.__str__() + "]: " + verify[index])

                if len(groups) > 0:
                    if permission_check is False:
                        return self.respond("PERMISSION DENIED", 403)
                    else:
                        if groups[0] == "None":
                            groups = []

                        groups_dao = GroupsDao(self.db)
                        await groups_dao.set_users_groups(user.id, groups)
            else:
                return self.respond("RECEIVED E-MAIL WHICH DOES NOT BELONG TO A USER", 404)

        return self.respond("USER(S) UPDATED")

    @tornado.web.authenticated
    async def delete(self):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, "delete_users")

        if permission_check is False:
            self.respond("PERMISSION DENIED", 403)

        session = self.current_user
        emails = self.get_arguments("emails[]")

        dao = UsersDao(self.db)

        for email in emails:
            if email == session.user.email:
                return self.respond("USER IS NOT ALLOWED TO MASS DELETE THEMSELVES", 405)

            user = await dao.get_user_by_email(email)
            if user is not None:
                await dao.remove_user(user.id)

        return self.respond("SUCCESSFULLY DELETED USER(S)")
