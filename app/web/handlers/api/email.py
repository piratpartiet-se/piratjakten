from uuid import UUID
from app.web.handlers.base import BaseHandler
from app.database.dao.email import EmailDao


class APIVerifyEmailHandler(BaseHandler):
    async def get(self, verify_id: UUID):
        verify_id = self.check_uuid(verify_id)
        if verify_id is None:
            return self.respond("VERIFY UUID IS MISSING", 400, show_error_page=True)

        email_dao = EmailDao(self.db)

        result = await email_dao.verify_email_by_link(verify_id)

        if result is True:
            self.render("success/email_verified.html", admin=False)
        else:
            return self.respond("VERIFY UUID WAS NOT ASSOCIATED WITH ANY E-MAIL", 404)