import re
import tornado.web

from uuid import UUID
from app.config import Config
from app.database.dao.users import UsersDao
from app.database.dao.codes import CodesDao
from app.web.handlers.base import BaseHandler


class APICodesHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self, category_id: UUID):
        dao = UsersDao(self.db)
        permission_check = await dao.check_user_permission(self.current_user.user.id, 'category_management')

        if permission_check is False:
            return self.respond("PERMISSION DENIED", 403)

        draw = self.get_argument("draw")
        start = self.get_argument("start")
        length = self.get_argument("length")
        global_search = self.get_argument("search[value]")
        order_column = self.get_argument("order[0][column]")
        order_dir = self.get_argument("order[0][dir]")

        order_column = self.get_argument("columns[" + order_column + "][data]")

        draw = int(draw)
        start = int(start)
        length = int(length)

        dao = CodesDao(self.db)

        if length > 0:
            code_list = await dao.get_codes_in_category(category_id, global_search, start, length, order_column,
                                                        order_dir)

            config = Config.get_config()
            code_prefix = config.get("WebServer", "codes_filepath")

            for code in code_list:
                code["image"] = re.sub(r'^' + re.escape(code_prefix), '', code["image"])
        else:
            return self.respond("LENGTH IS SMALLER THAN 0", 400)

        records_total = await dao.get_code_count_by_category(category_id)
        if global_search is None or global_search == "":
            records_filtered = records_total
        else:
            records_filtered = await dao.get_code_count_by_category(category_id, global_search)

        response = {
            'success': True,
            'draw': draw,
            'recordsTotal': records_total,
            'recordsFiltered': records_filtered,
            'data': code_list
        }

        return self.write(response)
