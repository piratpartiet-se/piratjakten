from app.logger import logger
import tornado.web

from uuid import UUID
from app.web.handlers.base import BaseHandler
from app.database.dao.bonuses import BonusesDao
from app.database.dao.claims import ClaimsDao
from app.database.dao.scans import ScansDao
from app.database.dao.rules import RulesDao


class APIScanHandler(BaseHandler):
    @tornado.web.authenticated
    async def post(self, code_id: UUID):
        code_id = self.check_uuid(code_id)
        if code_id is None:
            return self.respond("CODE UUID IS MISSING", 400)

        scans_dao = ScansDao(self.db)
        claim_dao = ClaimsDao(self.db)
        rules_dao = RulesDao(self.db)

        user_id = self.current_user.user.id
        longitude = self.get_argument("position[longitude]", None)
        latitude = self.get_argument("position[latitude]", None)

        if longitude == "" or longitude == "GEOLOCATION NOT SUPPORTED":
            longitude = None
            latitude = None
        if latitude == "" or latitude == "GEOLOCATION NOT SUPPORTED":
            longitude = None
            latitude = None

        result = await rules_dao.is_user_cooldown_not_active(code_id, user_id)
        if result is True:
            claim = await claim_dao.get_claim_by_code_id(code_id)

            if longitude is None or latitude is None:
                logger.debug("Position was not provided")
            else:
                logger.debug("Longitude: " + longitude + "\nLatitude: " + latitude)
                longitude = float(longitude)
                latitude = float(latitude)

            if claim is None:
                points = await rules_dao.award_points_for_claim(code_id, user_id, claim_dao)
                claim = await claim_dao.create_claim(user_id, code_id, points, longitude, latitude)

                if claim is None:
                    logger.critical("Claim could not be created!\n" +
                                    "User: " + user_id.__str__() + "\n" +
                                    "Code: " + code_id.__str__())
                    return self.respond("SOMETHING WENT WRONG WHEN TRYING TO CLAIM CODE", 500)

                logger.info("Claim created for user: " + user_id.__str__())

                return self.respond("CODE CLAIMED SUCCESSFULLY", 201)
            else:
                owner = claim.user_claim_id
                points = await rules_dao.award_points_for_scan(code_id, user_id, scans_dao)
                scan = await scans_dao.create_scan(user_id, code_id, points, longitude, latitude)

                if user_id != owner:
                    bonuses_dao = BonusesDao(self.db)
                    points = await rules_dao.award_points_for_bonus(code_id, owner, bonuses_dao, scans_dao)

                    await bonuses_dao.create_bonus(owner, scan.id, points)

                    logger.info("Bonus created for user: " + owner.__str__())

            logger.info("Scan created for user: " + user_id.__str__())

            return self.respond("SCAN SUCCESSFULLY REGISTERED", 201)
        else:
            logger.debug("User: " + user_id.__str__() + ", tried to scan code: " + code_id.__str__() +
                          "but cooldown is active.")
            return self.respond("COOLDOWN ACTIVE", 200)