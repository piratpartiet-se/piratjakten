from uuid import UUID
from configparser import ConfigParser
from app.config import Config

import os
import errno
import qrcode
import qrcode.image.svg


def generate_qrcodes(amount: int, code_ids: list, category_id: UUID) -> [qrcode.image.svg]:
    config = Config.get_config()  # type: ConfigParser

    images = []

    for i in range(amount):
        # TODO: generate a short url with: http://pir.at
        url = config.get("WebServer", "url") + "/api/code/" + code_ids[i].__str__()
        img = _generate_qrcode(url)
        filename = config.get("WebServer", "codes_filepath") + category_id.__str__() + "/" + code_ids[i].__str__() + ".svg"

        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        with open(filename, 'wb+') as f:
            img.save(f)

        images.append(filename)

    return images


def _generate_qrcode(url: str):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_M,
        box_size=20,
        border=4,
    )
    qr.add_data(url)
    qr.make(fit=True)

    factory = qrcode.image.svg.SvgPathImage

    img = qr.make_image(fill_color="black", back_color="white", image_factory=factory)

    return img
