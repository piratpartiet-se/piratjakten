from app.logger import logger

from datetime import datetime, timedelta
from typing import Union
from uuid import uuid4, UUID

from configparser import ConfigParser
from app.config import Config

from asyncpg import Connection
from asyncpg.pool import Pool

from app.web.qrgenerator import generate_qrcodes
from app.models import Code, Category


class CodesDao:
    def __init__(self, pool: Pool):
        self.pool = pool
        # TODO: add caching optimization for users, categories and codes

    async def create_codes(self, category_id: UUID, no: int) -> [Code]:
        codes = []
        for _ in range(no):
            code = await self.create_code(category_id)
            codes.append(code)
        return codes

    async def create_code(self, category_id: UUID) -> Code:
        sql = 'INSERT INTO codes (id, url, image, created, "category") VALUES ($1, $2, $3, $4, $5);'
        config = Config.get_config()  # type: ConfigParser

        code_uid = uuid4()
        url = config.get("WebServer", "url") + "/api/code/" + code_uid.__str__()
        image = generate_qrcodes(1, [code_uid], category_id)[0]
        created = datetime.utcnow()

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, code_uid, url, image, created, category_id)

        sql = 'SELECT name, description, info_url, created FROM categories WHERE id = $1'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, category_id)

        code = Code()
        code.id = code_uid
        code.url = url
        code.image = image
        code.created = created
        code.category = Category()
        code.category.id = category_id
        code.category.name = row["name"]
        code.category.description = row["description"]
        code.category.info_url = row["info_url"]
        code.category.created = row["created"]

        return code

    async def get_code_by_id(self, code_id: UUID) -> Union[Code, None]:
        sql = 'SELECT id, url, image, created, "category" from codes WHERE id = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, code_id)

        if row is None:
            return None

        sql = 'SELECT name, description, info_url, created FROM categories WHERE id = $1'

        async with self.pool.acquire() as con:  # type: Connection
            category_row = await con.fetchrow(sql, row["category"])

        code = Code()
        code.id = code_id
        code.url = row["url"]
        code.image = row["image"]
        code.created = row["created"]
        code.category = Category()
        code.category.id = row["category"]
        code.category.name = category_row["name"]
        code.category.description = category_row["description"]
        code.category.info_url = category_row["info_url"]
        code.category.created = category_row["created"]

        return code

    async def get_codes_in_category(self, category_id: UUID, search: str, start: int, length: int, order_column: str,
                                    order_dir: str) -> [dict]:
        """
        Get a list containing code data for codes in a given category, used for DataTables
        :return: A list filled dicts
        """

        if order_dir == "asc":
            order_dir = "ASC"
        else:
            order_dir = "DESC"

        if order_column != "id" and order_column != "image" and order_column != "created":
            order_column = "created"

        if search == "":
            sql = """ SELECT id, image, created FROM codes WHERE "category" = $1
                      ORDER BY """ + order_column + " " + order_dir + """
                      LIMIT $2 OFFSET $3"""

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, category_id, length, start)
        else:
            search = "%"+search+"%"
            sql = """ SELECT id, image, created FROM codes WHERE "category" = $1 AND (
                      image LIKE $2
                      OR to_char(created, 'YYYY-MM-DD HH24:MI:SS.US') LIKE $2
                      )
                      ORDER BY """ + order_column + " " + order_dir + """
                      LIMIT $3 OFFSET $4"""

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, category_id, search, length, start)

        codes = []
        for row in rows:
            code = {
                "id": row["id"].__str__(),
                "image": row["image"],
                "created": row["created"].isoformat(' ')
            }
            codes.append(code)

        return codes

    async def get_code_count_by_category(self, category_id: UUID, global_search: str = None) -> int:
        """
        Get how many codes currently exist in a category group
        :return: An int with the current code count in the category
        """
        if global_search is None:
            sql = 'SELECT count(*) AS codes FROM codes WHERE "category" = $1;'

            async with self.pool.acquire() as con:  # type: Connection
                row = await con.fetchrow(sql, category_id)
        else:
            global_search = "%" + global_search + "%"
            sql = """SELECT count(*) AS codes FROM codes WHERE "category" = $1 AND (
                         image LIKE $2 OR
                         to_char(created, 'YYYY-MM-DD HH24:MI:SS.US') LIKE $2
                     );"""

            async with self.pool.acquire() as con:  # type: Connection
                row = await con.fetchrow(sql, category_id, global_search)

        return row["codes"]
