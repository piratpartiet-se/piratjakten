from app.logger import logger

from typing import Union
from datetime import datetime
from uuid import uuid4, UUID
from asyncpg import Connection, PostgresError
from asyncpg.pool import Pool
from app.models import Scan


class ScansDao:
    def __init__(self, pool: Pool):
        self.pool = pool

    async def create_scan(self, user_id: Union[UUID, None], code_id: UUID, points: int, longitude: float = None,
                          latitude: float = None) -> Union[Scan, None]:
        scan_id = uuid4()
        created = datetime.utcnow()

        try:
            if latitude is None or longitude is None:
                sql = 'INSERT INTO scans (id, user_scan, "code", points, created) VALUES ($1, $2, $3, $4, $5);'

                async with self.pool.acquire() as con:  # type: Connection
                    await con.execute(sql, scan_id, user_id, code_id, points, created)
            else:
                sql = 'INSERT INTO scans (id, user_scan, "code", points, created, coordinates) VALUES ($1, $2, $3, $4, $5, point($6, $7));'

                async with self.pool.acquire() as con:  # type: Connection
                    await con.execute(sql, scan_id, user_id, code_id, points, created, longitude, latitude)
        except PostgresError:
            logger.warning("Code with UID: " + code_id.__str__() + " not found")
            return None

        scan = Scan()
        scan.id = scan_id
        scan.user_scan_id = user_id
        scan.code_id = code_id
        scan.points = points
        scan.created = created

        if longitude is not None:
            scan.longitude = longitude
        if latitude is not None:
            scan.latitude = latitude

        return scan

    async def get_scan_by_id(self, scan_id: UUID) -> Union[Scan, None]:
        if scan_id is None:
            return None

        sql = 'SELECT user_scan, "code", points, created, coordinates FROM scans WHERE id = $1'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, scan_id)

        scan = Scan()
        scan.id = scan_id
        scan.user_scan_id = row["user_scan"]
        scan.code_id = row["code"]
        scan.points = row["points"]
        scan.created = row["created"]

        if row["coordinates"] is not None:
            scan.longitude = row["coordinates"].x
            scan.latitude = row["coordinates"].y

        return scan

    async def get_scans(self, amount: Union[int, None] = None):
        sql = 'SELECT id, "code", points, created, coordinates FROM scans ORDER BY created DESC'

        if amount is not None:
            sql += ' LIMIT $1;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, amount)
        else:
            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql)

        scans = []
        for row in rows:
            scan = Scan()
            scan.id = row["id"]
            scan.code_id = row["code"]
            scan.points = row["points"]
            scan.created = row["created"]

            if row["coordinates"] is not None:
                scan.longitude = row["coordinates"].x
                scan.latitude = row["coordinates"].y

            scans.append(scan)

        return scans

    async def get_scans_by_user_id(self, user_id: UUID, amount: int=None) -> list:
        sql = 'SELECT id, "code", points, created, coordinates FROM scans WHERE user_scan = $1 ORDER BY created DESC'

        if amount is not None:
            sql += ' LIMIT $2;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, user_id, amount)
        else:
            sql += ';'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, user_id)

        scans = []
        for row in rows:
            scan = Scan()
            scan.id = row["id"]
            scan.user_scan_id = user_id
            scan.code_id = row["code"]
            scan.points = row["points"]
            scan.created = row["created"]

            if row["coordinates"] is not None:
                scan.longitude = row["coordinates"].x
                scan.latitude = row["coordinates"].y

            scans.append(scan)

        return scans

    async def get_scans_by_code_id(self, code_id: UUID, amount: int = None) -> list:
        sql = 'SELECT id, user_scan, points, created, coordinates FROM scans WHERE "code" = $1 ORDER BY created DESC'

        if amount is not None:
            sql += ' LIMIT $2;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, code_id, amount)
        else:
            sql += ';'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, code_id)

        scans = []
        for row in rows:
            scan = Scan()
            scan.id = row["id"]
            scan.user_scan_id = row["user_scan"]
            scan.code_id = code_id
            scan.points = row["points"]
            scan.created = row["created"]

            if row["coordinates"] is not None:
                scan.longitude = row["coordinates"].x
                scan.latitude = row["coordinates"].y

            scans.append(scan)

        return scans

    async def get_scan_count_by_user_id(self, user_id: UUID) -> int:
        sql = 'SELECT count(*) AS scans FROM scans WHERE user_scan = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, user_id)

        return row["scans"]

    async def update_points_for_scan(self, scan_id: UUID, points: int) -> int:
        sql = 'UPDATE scans SET points = $1 WHERE id = $2;'

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, points, scan_id)

        return points
