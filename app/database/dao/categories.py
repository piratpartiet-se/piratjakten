from datetime import datetime
from typing import Union
from uuid import uuid4, UUID

from asyncpg import Connection
from asyncpg.pool import Pool

from app.models import Category

class CategoriesDao:
    def __init__(self, pool: Pool):
        self.pool = pool

    async def create_category(self, name:str, description:str="", info_url:str="https://www.piratpartiet.se/") -> Category:
        sql = "INSERT INTO categories (id, name, description, info_url, created) VALUES ($1, $2, $3, $4, $5);"

        category_uid = uuid4()
        created = datetime.utcnow()

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, category_uid, name, description, info_url, created)

        category = Category()
        category.id = category_uid
        category.name = name
        category.description = description
        category.info_url = info_url
        category.created = created

        return category

    async def get_category_by_code_id(self, code_id: UUID) -> Category:
        sql = 'SELECT id, name, description, info_url, created FROM categories WHERE id = (SELECT "category" FROM codes WHERE id = $1)'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, code_id)

        category = Category()
        category.id = row["id"]
        category.name = row["name"]
        category.description = row["description"]
        category.info_url = row["info_url"]
        category.created = row["created"]

        return category

    async def get_category_by_id(self, category_id: UUID) -> Category:
        return await self._get_category(category_id=category_id)

    async def get_category_by_name(self, name: str) -> Category:
        return await self._get_category(name=name)

    async def _get_category(self, category_id: UUID=None, name: str=None) -> Union[Category, None]:
        sql = "SELECT id, name, description, info_url, created FROM categories"

        if category_id is not None:
            sql += " WHERE id = $1;"
            search_with = category_id
        elif name is not None:
            sql += " WHERE name = $1;"
            search_with = name
        else:
            return None

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, search_with)

        if row is None:
            return None

        category = Category()
        category.id = row["id"]
        category.name = row["name"]
        category.description = row["description"]
        category.info_url = row["info_url"]
        category.created = row["created"]

        return category

    async def get_categories(self) -> list:
        sql = "SELECT id, name, description, info_url, created FROM categories;"

        async with self.pool.acquire() as con:  # type: Connection
            rows = await con.fetch(sql)

        categories = []

        for row in rows:
            category = Category()
            category.id = row["id"]
            category.name = row["name"]
            category.description = row["description"]
            category.info_url = row["info_url"]
            category.created = row["created"]
            categories.append(category)

        return categories
