from app.logger import logger
from typing import Union
from datetime import datetime
from uuid import uuid4, UUID
from asyncpg import Connection
from asyncpg.pool import Pool
from app.models import Claim


class ClaimsDao:
    def __init__(self, pool: Pool):
        self.pool = pool

    async def create_claim(self, user_id: UUID, code_id: UUID, points: int, longitude: float = None, latitude: float = None) -> Claim:
        id = uuid4()
        created = datetime.utcnow()

        try:
            if latitude is None or longitude is None:
                sql = 'INSERT INTO claims (id, user_claim, "code", points, created) VALUES ($1, $2, $3, $4, $5);'

                async with self.pool.acquire() as con:  # type: Connection
                    await con.execute(sql, id, user_id, code_id, points, created)
            else:
                sql = 'INSERT INTO claims (id, user_claim, "code", points, created, coordinates) VALUES ($1, $2, $3, $4, $5, point($6, $7));'

                async with self.pool.acquire() as con:  # type: Connection
                    await con.execute(sql, id, user_id, code_id, points, created, longitude, latitude)
        except Exception as exc:
            logger.warning("Something went wrong when claim was being created:\n" + str(exc))
            return None

        claim = Claim()
        claim.id = id
        claim.user_claim_id = user_id
        claim.code_id = code_id
        claim.points = points
        claim.created = created

        if longitude is not None:
            claim.longitude = longitude
        if latitude is not None:
            claim.latitude = latitude

        return claim

    async def get_claims(self, amount: Union[int, None] = None) -> list:
        sql = 'SELECT id, user_claim, "code", points, created, coordinates FROM claims ORDER BY created DESC'

        if amount is not None:
            sql += ' LIMIT $1;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, amount)
        else:
            sql += ';'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql)

        claims = []
        for row in rows:
            claim = Claim()
            claim.id = row["id"]
            claim.user_claim_id = row["user_claim"]
            claim.code_id = row["code"]
            claim.points = row["points"]
            claim.created = row["created"]

            if row["coordinates"] is not None:
                claim.longitude = row["coordinates"].x
                claim.latitude = row["coordinates"].y

            claims.append(claim)

        return claims

    async def get_claims_by_user_id(self, user_id: UUID, amount: int = None) -> list:
        sql = 'SELECT id, "code", points, created, coordinates FROM claims WHERE user_claim = $1 ORDER BY created DESC'

        if amount is not None:
            sql += ' LIMIT $2;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, user_id, amount)
        else:
            sql += ';'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, user_id)

        claims = []
        for row in rows:
            claim = Claim()
            claim.id = row["id"]
            claim.user_claim_id = user_id
            claim.code_id = row["code"]
            claim.points = row["points"]
            claim.created = row["created"]

            if row["coordinates"] is not None:
                claim.longitude = row["coordinates"].x
                claim.latitude = row["coordinates"].y

            claims.append(claim)

        return claims

    async def get_claim_by_code_id(self, code_id: UUID) -> Claim:
        sql = 'SELECT id, user_claim, points, created, coordinates FROM claims WHERE "code" = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, code_id)

        if row is None:
            return None

        claim = Claim()
        claim.id = row["id"]
        claim.user_claim_id = row["user_claim"]
        claim.code_id = code_id
        claim.points = row["points"]
        claim.created = row["created"]

        if row["coordinates"] is not None:
            claim.longitude = row["coordinates"].x
            claim.latitude = row["coordinates"].y

        return claim

    async def update_points_for_claim(self, claim_id: UUID, points: int) -> int:
        sql = 'UPDATE claims SET points = $1 WHERE id = $2;'

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, points, claim_id)

        return points
