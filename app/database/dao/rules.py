import math

from datetime import datetime, timedelta
from uuid import uuid4, UUID
from asyncpg import Connection
from asyncpg.exceptions import NoDataFoundError
from asyncpg.pool import Pool
from app.logger import logger
from app.models import Rule
from app.database.dao.scans import ScansDao
from app.database.dao.claims import ClaimsDao
from app.database.dao.bonuses import BonusesDao
from typing import Union


class RulesDao:
    def __init__(self, pool: Pool):
        self.pool = pool

    async def create_rule(self, start_date: datetime, end_date: datetime, points: list, cooldown: timedelta,
                          reference_is_category: bool) -> Rule:
        # TODO: Finish implementation of create rule function
        sql = 'INSERT INTO rules (id, start_date, end_date, points, cooldown, created, '

        if reference_is_category:
            sql += '"category"'
        else:
            sql += '"code"'

        sql += 'VALUES ($1, $2, $3, $4, $5, $6);'

        rule_id = uuid4()
        created = datetime.utcnow()

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, rule_id, start_date, end_date, points, cooldown, created)

        rule = Rule()
        rule.id = rule_id
        rule.start_date = start_date
        rule.end_date = end_date
        rule.points = points
        rule.user_cooldown = cooldown
        rule.created = created

        return rule

    async def award_points_for_scan(self, code_id: UUID, user_id: UUID, scan_dao: ScansDao) -> int:
        """
        Get how many points a user will get for a new claim for the code the provided
        Updates the total points for the user in the database also
        :return: An int with the points awarded to the user
        """

        rule = await self.get_rule_by_name("Standard")

        if len(rule.scan_points) is 0:
            return 0

        scans = await scan_dao.get_scans_by_code_id(code_id)

        count = 0
        for scan in scans:
            if scan.user_scan_id == user_id:
                count += 1

        if count >= len(rule.scan_points):
            count = len(rule.scan_points) - 1

        points = rule.scan_points[count]

        await self.add_points_for_user(user_id, points)

        return points

    async def award_points_for_claim(self, code_id: UUID, user_id: UUID, claim_dao: ClaimsDao) -> int:
        """
        Get how many points a user will get for a new claim for the code the provided
        Updates the total points for the user in the database also
        :return: An int with the points awarded to the user
        """

        rule = await self.get_rule_by_name("Standard")
        points = rule.claim_points

        if await claim_dao.get_claim_by_code_id(code_id) is None:
            await self.add_points_for_user(user_id, points)
        else:
            return 0

        return points

    async def award_points_for_bonus(self, code_id: UUID, user_id: UUID, bonus_dao: BonusesDao,
                                     scan_dao: ScansDao) -> int:
        """
        Get how many bonus points a user will get for a code scanned by another user
        Updates the total points for the user in the database also
        :return: An int with the points awarded to the user
        """

        rule = await self.get_rule_by_name("Standard")

        if len(rule.bonus_points) is 0:
            return 0

        bonuses = await bonus_dao.get_bonuses_by_user_id(user_id)

        count = 0
        for bonus in bonuses:
            scan = await scan_dao.get_scan_by_id(bonus.scan_id)
            if scan.code_id == code_id:
                count += 1

        if count >= len(rule.bonus_points):
            count = len(rule.bonus_points) - 1

        points = rule.bonus_points[count]

        await self.add_points_for_user(user_id, points)

        return points

    async def award_points_for_non_user_bonus(self, code_id: UUID, user_id: UUID) -> int:
        """
        Get how many bonus points a user will get for a code scanned by a non-user
        Updates the total points for the user in the database also
        :return: An int with the points awarded to the user
        """

        rule = await self.get_rule_by_name("Standard")
        points = rule.non_user_points
        await self.add_points_for_user(user_id, points)

        return points

    async def get_rule_by_id(self, rule_id: UUID) -> Rule:
        sql = (
            'SELECT name, start_date, end_date, claim_points, scan_points, bonus_points, non_user_points, ' +
            'user_cooldown, non_user_cooldown, created, "category", "code" FROM rules WHERE id = $1;'
        )

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, rule_id)

        rule = Rule()
        rule.id = rule_id
        rule.name = row["name"]
        rule.start_date = row["start_date"]
        rule.end_date = row["end_date"]
        rule.claim_points = row["claim_points"]
        rule.scan_points = row["scan_points"]
        rule.bonus_points = row["bonus_points"]
        rule.non_user_points = row["non_user_points"]
        rule.cooldown = row["user_cooldown"]
        rule.non_user_cooldown = row["non_user_cooldown"]
        rule.created = row["created"]
        rule.category_id = row["category"]
        rule.code_id = row["code"]

        return rule

    async def get_rule_by_name(self, name: str) -> Rule:
        sql = (
            'SELECT id, start_date, end_date, claim_points, scan_points, bonus_points, non_user_points, ' +
            'user_cooldown, non_user_cooldown, created, "category", "code" FROM rules WHERE name = $1;'
        )

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, name)

        rule = Rule()
        rule.id = row["id"]
        rule.name = name
        rule.start_date = row["start_date"]
        rule.end_date = row["end_date"]
        rule.claim_points = row["claim_points"]
        rule.scan_points = row["scan_points"]
        rule.bonus_points = row["bonus_points"]
        rule.non_user_points = row["non_user_points"]
        rule.cooldown = row["user_cooldown"]
        rule.non_user_cooldown = row["non_user_cooldown"]
        rule.created = row["created"]
        rule.category_id = row["category"]
        rule.code_id = row["code"]

        return rule

    async def get_placements(self, amount, user_id: UUID = None) -> dict:
        """
        Get a list of users based on their score, higher score meaning closer to the beginning of the list
        The first position of the list is reserved for the offset, meaning which position you start counting from
        :return: A list filled with the offset in the beginning the rest user objects, ordered by score
        """
        if user_id is None:
            sql = (
                'SELECT "user", points, level, RANK () OVER (ORDER BY points DESC) AS placement FROM placements ' +
                'ORDER BY points DESC LIMIT $1;'
            )

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, amount)

            placements = {}
            for row in rows:
                placements[row["user"]] = [row["placement"], row["points"], row["level"]]

            return placements
        else:
            sql = (
                'SELECT rank.* FROM (SELECT "user", points, level, RANK () OVER(ORDER BY points DESC) AS placement ' +
                'FROM placements) AS rank WHERE rank."user" = $1;'
            )

            async with self.pool.acquire() as con:  # type: Connection
                row = await con.fetchrow(sql, user_id)

            placements = {}
            offset = row["placement"] - 1

            if amount is 1:
                placements[row["user"]] = [row["placement"], row["points"], row["level"]]
                return placements

            offset = offset - math.ceil(amount / 2)

            if offset < 0:
                offset = 0

            sql = (
                'SELECT "user", points, level, RANK () OVER (ORDER BY points DESC) AS placement FROM placements ' +
                'ORDER BY points DESC LIMIT $1 OFFSET $2;'
            )

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, amount, offset)

            for row in rows:
                placements[row["user"]] = [row["placement"], row["points"], row["level"]]

            return placements

    async def get_user_score(self, user_id:UUID) -> Union[int, None]:
        sql = 'SELECT points FROM placements WHERE "user" = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, user_id)

        if row is None:
            return None

        return row["points"]

    async def get_user_cooldown(self, name: str) -> Union[timedelta, None]:
        sql = 'SELECT user_cooldown FROM rules WHERE name = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, name)

        if row is None:
            logger.warning('Rule with name: "' + name + '" was not found')
            return None

        return row["user_cooldown"]

    async def get_non_user_cooldown(self, name: str) -> timedelta:
        sql = 'SELECT non_user_cooldown FROM rules WHERE name = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, name)

        if row is None:
            logger.warning('Rule with name: "' + name + '" was not found')
            return None

        return row["non_user_cooldown"]

    async def recalculate_score(self, user_id: UUID) -> int:
        """
        Runs through the scans, bonuses and claims belonging to the user provided and calculates the total score.
        Also updates the database with scores for users and scans, this is the only function that does it.
        :return: An int, the calculated score for the user
        """

        rule = await self.get_rule_by_name("Standard")
        scan_dao = ScansDao(self.pool)
        claim_dao = ClaimsDao(self.pool)
        bonus_dao = BonusesDao(self.pool)

        scans = await scan_dao.get_scans_by_user_id(user_id)
        claims = await claim_dao.get_claims_by_user_id(user_id)
        bonuses = await bonus_dao.get_bonuses_by_user_id(user_id)

        if len(rule.scan_points) is 0 or len(rule.bonus_points) is 0:
            logger.debug("Default rule is flawed, returning 0")
            return 0

        points = 0

        logger.debug("Length of scan list: " + len(scans).__str__())
        logger.debug("Length of claim list: " + len(claims).__str__())
        logger.debug("Length of bonus list: " + len(bonuses).__str__())

        points += count_scans_points(scans, rule, scan_dao)
        points += count_claims_points(claims, rule, claim_dao)
        points += count_bonuses_points(bonuses, rule, bonus_dao)

        logger.info("User " + user_id.__str__() + " points were recalculated to: " + points.__str__())
        await self.set_points_for_user(user_id)

        return points

    async def is_user_cooldown_not_active(self, code_id: UUID, user_id: UUID) -> bool:
        time = await self._get_time_from_latest_activity(code_id, user_id)

        logger.debug("User: " + user_id.__str__() + "\n" +
                     "Code: " + code_id.__str__() + "\n" +
                     "Time from latest activity: " + time.__str__())

        user_cooldown = await self.get_user_cooldown("Standard")

        if user_cooldown is None:
            return True

        user_cooldown = user_cooldown.total_seconds()

        if time.total_seconds() > user_cooldown:
            return True

        logger.debug("User cooldown is active for user: " + user_id.__str__() + " with code: " + code_id.__str__() +
                     "\n" +
                     "Seconds remaining: " + (user_cooldown - time.total_seconds()).__str__())
        return False

    async def is_non_user_cooldown_not_active(self, code_id: UUID) -> bool:
        time = await self._get_time_from_latest_non_user_activity(code_id)

        logger.debug("Code: " + code_id.__str__() + "\n" +
                     "Time from latest activity: " + time.__str__())

        non_user_cooldown = await self.get_non_user_cooldown("Standard")

        if non_user_cooldown is None:
            return True

        non_user_cooldown = non_user_cooldown.total_seconds()

        if time.total_seconds() > non_user_cooldown:
            return True

        logger.debug("Non user cooldown is active for code: " + code_id.__str__() + "\n" +
                     "Seconds remaining: " + (non_user_cooldown - time.total_seconds()).__str__())
        return False

    async def set_points_for_user(self, user_id: UUID, points: int):
        level = get_level_by_points(points)

        sql = 'UPDATE placements SET points = $1, level = $2 WHERE "user" = $3;'

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, points, level, user_id)

        logger.debug("Updating points for user_id: " + user_id.__str__() + "\n" +
                     "New total: " + points.__str__() + "\n" +
                     "New level: " + level.__str__())

        return points

    async def add_points_for_user(self, user_id: UUID, points: int):
        sql = 'SELECT points FROM placements WHERE "user" = $1;'
        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, user_id)

        if row is None:
            logger.error("User with ID: " + user_id.__str__() + " was not found in placements table")
            return 0

        logger.debug("Adding " + points.__str__() + " points to user: " + user_id.__str__() +
                     "\nCurrent total: " + row["points"].__str__())

        points += row["points"]
        level = get_level_by_points(points)

        logger.debug("New total: " + points.__str__())
        logger.debug("New level: " + level.__str__())

        sql = 'UPDATE placements SET points = $1, level = $2 WHERE "user" = $3;'
        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, points, level, user_id)

        return points

    async def set_level_for_user(self, user_id: UUID, level: int) -> int:
        sql = 'UPDATE placements SET level = $1 IN placements WHERE "user" = $2;'

        try:
            async with self.pool.acquire() as con:  # type: Connection
                await con.execute(sql, level, user_id)
        except NoDataFoundError as exc:
            logger.error("User with ID: " + user_id.__str__() + " was not found")
            logger.error(exc)
            return None

        logger.debug("Level set to: " + level.__str__() + "for user: " + user_id.__str__())

        return level

    async def _get_time_from_latest_activity(self, code_id: UUID, user_id: UUID) -> timedelta:
        result = await self._get_time_from_latest_scan(code_id, user_id)

        if result is not None:
            return result

        result = await self._get_time_from_latest_claim(code_id, user_id)

        if result is not None:
            return result

        return timedelta.max

    async def _get_time_from_latest_non_user_activity(self, code_id: UUID) -> timedelta:
        result = await self._get_time_from_latest_scan(code_id, None)

        if result is not None:
            return result

        return timedelta.max

    async def _get_time_from_latest_claim(self, code_id: UUID, user_id: UUID) -> Union[timedelta, None]:
        sql = 'SELECT created FROM claims WHERE "code" = $1 AND user_claim = $2 ORDER BY created DESC LIMIT 1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, code_id, user_id)

        if row is None:
            return None

        date_scanned = row["created"]

        return datetime.utcnow() - date_scanned

    async def _get_time_from_latest_scan(self, code_id: UUID, user_id: Union[UUID, None]) -> Union[timedelta, None]:
        sql = 'SELECT created FROM scans WHERE "code" = $1 AND user_scan = $2 ORDER BY created DESC LIMIT 1;'

        if user_id is None:
            sql = 'SELECT created FROM scans WHERE "code" = $1 AND user_scan IS NULL ORDER BY created DESC LIMIT 1;'

            async with self.pool.acquire() as con:  # type: Connection
                row = await con.fetchrow(sql, code_id)
        else:
            async with self.pool.acquire() as con:  # type: Connection
                row = await con.fetchrow(sql, code_id, user_id)

        if row is None:
            return None

        date_scanned = row["created"]

        return datetime.utcnow() - date_scanned


def _roundup_to_nearest_100(x):
    if x is 0:
        return 100

    return int(math.ceil(x / 100.0)) * 100


def get_level_by_points(points: int) -> int:
    level = 1

    if type(points) != int:
        logger.error("int was not passed to 'get_level_by_points', throwing TypeError")

        raise TypeError("int was expected as a parameter for 'get_level_by_points' but instead a " +
                        type(points).__name__ + " was given")

    while points > 100:
        points -= 100
        level += 1

    return level


def get_points_to_next_level(points: int) -> int:
    next_level = _roundup_to_nearest_100(points)

    logger.debug("Points to next level, with " + points.__str__() + " points, is: " + next_level.__str__())

    return next_level


async def count_scans_points(scans: list, rule: Rule, scan_dao: ScansDao) -> int:
    points = 0
    cache = {}

    if len(rule.scan_points) is 0:
        return points

    for scan in scans:
        if scan.code_id not in cache:
            index = 0

            points += rule.scan_points[index]
            await scan_dao.update_points_for_scan(scan.id, rule.scan_points[index])

            cache[scan.code_id] = 1
        else:
            index = cache[scan.code_id]

            if index >= len(rule.scan_points):
                index = len(rule.scan_points)

            points += rule.scan_points[index]
            await scan_dao.update_points_for_scan(scan.id, rule.scan_points[index])

            cache[scan.code_id] += 1

    return points


async def count_claims_points(claims: list, rule, claim_dao: ClaimsDao) -> int:
    points = 0

    for claim in claims:
        await claim_dao.update_points_for_claim(claim.id, rule.claim_points)
        points += rule.claim_points

    logger.debug("Counted " + len(claims).__str__() + " claims which added up to: " + points.__str__() + " points")

    return points


async def count_bonuses_points(bonuses: list, rule, bonus_dao: BonusesDao, scan_dao: ScansDao) -> int:
    points = 0
    cache = {}

    if len(rule.bonus_points) is 0:
        return points

    for bonus in bonuses:
        scan = await scan_dao.get_scan_by_id(bonus.scan_id)

        if scan is None:
            continue
        elif scan.user_scan_id is None:
            points += rule.non_user_points
            await bonus_dao.update_points_for_bonus(bonus.id, rule.non_user_points)
            continue

        if bonus.code_id not in cache:
            index = 0

            points += rule.bonus_points[index]
            await bonus_dao.update_points_for_bonus(bonus.id, rule.bonus_points[index])

            cache[bonus.code_id] = 1
        else:
            index = cache[bonus.code_id]

            if index >= len(rule.bonus_points):
                index = len(rule.bonus_points)

            points += rule.bonus_points[index]
            await bonus_dao.update_points_for_bonus(bonus.id, rule.bonus_points[index])

            cache[bonus.code_id] += 1

    return points
