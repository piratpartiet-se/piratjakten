from datetime import datetime
from typing import Union
from uuid import uuid4, UUID

from asyncpg import Connection, UniqueViolationError
from asyncpg.pool import Pool

from app.logger import logger
from app.models import Group


class GroupsDao:
    def __init__(self, pool: Pool):
        self.pool = pool

    async def create_group(self, name: str, level: int, permissions_values: dict = None) -> Group:
        sql = "INSERT INTO groups (id, name, level, created) VALUES ($1, $2, $3, $4);"

        group_id = uuid4()
        created = datetime.utcnow()

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, group_id, name, level, created)

        sql = 'INSERT INTO groups_permissions ("group", "permission") VALUES ($1, $2);'

        if permissions_values is not None:
            for p, v in permissions_values.items():
                if v is True:
                    async with self.pool.acquire() as con:  # type: Connection
                        await con.execute(sql, group_id, p)

        group = Group()
        group.id = group_id
        group.name = name
        group.level = level
        group.created = created

        return group

    async def get_group_by_id(self, id: UUID) -> Union[Group, None]:
        """
        Get a group by name
        :return: A Group object or None if nothing is found
        """

        sql = "SELECT name, level, created FROM groups WHERE id = $1;"

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, id)

        if row is None:
            return None

        group = Group()
        group.id = id
        group.name = row["name"]
        group.level = row["level"]
        group.created = row["created"]

        return group

    async def get_group_by_name(self, name: str) -> Union[Group, None]:
        """
        Get a group by name
        :return: A Group object or None if nothing is found
        """

        sql = "SELECT id, name, level, created FROM groups WHERE name = $1;"

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, name)

        if row is None:
            return None

        group = Group()
        group.id = row["id"]
        group.name = row["name"]
        group.level = row["level"]
        group.created = row["created"]

        return group

    async def get_groups_by_user_id(self, user_id:UUID) -> [Group]:
        """
        Get a list of Group models that has a member with the provided user ID
        :return: A list filled with Group models
        """
        sql = 'SELECT "group" FROM users_groups WHERE "user" = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            ids = await con.fetch(sql, user_id)

        groups = []
        for id in ids:
            sql = 'SELECT id, name, level, created FROM groups WHERE id = $1;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, id["group"])
            
            for row in rows:
                group = Group()
                group.id = row["id"]
                group.name = row["name"]
                group.level = row["level"]
                group.created = row["created"]

                groups.append(group)

        return groups

    async def get_groups(self, search: str, start: int, length: int, order_column: str, order_dir: str) -> [dict]:
        """
        Get a list containing user data, used for DataTables
        :return: A list filled dicts
        """

        if order_dir == "asc":
            order_dir = "ASC"
        else:
            order_dir = "DESC"

        if order_column != "name" and order_column != "level" and order_column != "created":
            order_column = "name"

        if search == "":
            sql = """ SELECT id, name, level, created FROM groups
                      ORDER BY """ + order_column + " " + order_dir + """
                      LIMIT $1 OFFSET $2"""

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, length, start)
        else:
            search = "%"+search+"%"
            sql = """ SELECT id, name, level, created FROM groups
                      WHERE name LIKE $1
                      OR to_char(created, 'YYYY-MM-DD HH24:MI:SS.US') LIKE $1
                      ORDER BY """ + order_column + " " + order_dir + """
                      LIMIT $2 OFFSET $3"""

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, search, length, start)

        groups = []
        for row in rows:
            sql = 'SELECT count("user") AS size FROM users_groups WHERE "group" = $1'

            async with self.pool.acquire() as con:  # type: Connection
                users_in_group = await con.fetchrow(sql, row["id"])

            sql = 'SELECT "permission" FROM groups_permissions WHERE "group" = $1'

            async with self.pool.acquire() as con:  # type: Connection
                group_permissions_raw = await con.fetch(sql, row["id"])

            group_permissions = []

            for g in group_permissions_raw:
                group_permissions.append(g["permission"])
            
            sql = 'SELECT id, name FROM permissions'

            async with self.pool.acquire() as con:  # type: Connection
                permissions = await con.fetch(sql)

            group = {
                "uuid": row["id"].__str__(),
                "name": row["name"],
                "size": users_in_group["size"],
                "level": row["level"],
                "created": row["created"].isoformat(' '),
                "permissions": {}
            }

            for p in permissions:
                group["permissions"][p["id"]] = {
                    "value": p["id"] in group_permissions,
                    "name": p["name"]
                }

            groups.append(group)

        return groups

    async def get_all_groups(self) -> [Group]:
        """
        Get a list of Group models
        :return: A list filled with Group models
        """
        sql = 'SELECT id, name, level, created FROM groups ORDER BY name ASC;'

        async with self.pool.acquire() as con:  # type: Connection
            rows = await con.fetch(sql)

        groups = []
        for row in rows:
            group = Group()
            group.id = row["id"]
            group.name = row["name"]
            group.level = row["level"]
            group.created = row["created"]

            groups.append(group)

        return groups

    async def get_group_count(self, global_search: str = None) -> int:
        """
        Get how many groups currently exist
        :return: An int with the current group count
        """
        if global_search is None:
            sql = "SELECT count(*) AS groups FROM groups;"

            async with self.pool.acquire() as con:  # type: Connection
                row = await con.fetchrow(sql)

            return row["groups"]
        else:
            global_search = "%" + global_search + "%"

            sql = """SELECT count(*) AS groups FROM groups
                     WHERE name LIKE $1
                     OR to_char(created, 'YYYY-MM-DD HH24:MI:SS.US') LIKE $1;"""

            async with self.pool.acquire() as con:  # type: Connection
                row = await con.fetchrow(sql, global_search)

            return row["groups"]

    async def get_permissions(self) -> dict:
        """
        Get all available permissions
        :return: A dict with all permissions
        """
        sql = "SELECT * FROM permissions;"

        async with self.pool.acquire() as con:  # type: Connection
            rows = await con.fetch(sql)

        permissions = {}

        for row in rows:
            permissions[row["id"]] = row["name"]

        return permissions

    async def update_group(self, group_id: UUID, name: str, level: int):
        sql = "UPDATE groups SET name = $1, level = $2 WHERE id = $3"

        try:
            async with self.pool.acquire() as con:  # type: Connection
                await con.execute(sql, name, level, group_id)
        except Exception as e:
            logger.error("Failed to update group: " + str(e))
            return None

        sql = "SELECT id, name, level, created FROM groups WHERE id = $1"

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, group_id)

        group = Group()
        group.id = row["id"]
        group.name = row["name"]
        group.level = row["level"]
        group.created = row["created"]

        return group

    async def delete_group(self, group_id: UUID) -> bool:
        sql = 'DELETE FROM groups_permissions WHERE "group" = $1;'
        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, group_id)

        sql = 'DELETE FROM users_groups WHERE "group" = $1;'
        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, group_id)

        sql = 'DELETE FROM groups WHERE id = $1;'
        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, group_id)

        return True

    async def set_permissions_for_group(self, group_id: UUID, permission_values: dict) -> dict:
        """
        Set permissions for group
        :return: A dict with all permissions
        """

        permissions = {}
        logger.debug("Setting permissions for group: " + group_id.__str__())

        for permission, value in permission_values.items():
            logger.debug("Permission: " + permission + "=" + value.__str__())

            if value is False:
                sql = 'DELETE FROM groups_permissions WHERE "group" = $1 AND "permission" = $2;'

                async with self.pool.acquire() as con:  # type: Connection
                    await con.execute(sql, group_id, permission)

                permissions[permission] = False
            elif value is True:
                sql = 'INSERT INTO groups_permissions ("group", "permission") VALUES ($1, $2);'

                try:
                    async with self.pool.acquire() as con:  # type: Connection
                        await con.execute(sql, group_id, permission)
                except UniqueViolationError as exc:
                    logger.debug(exc)
                    logger.warning("Group: " + group_id.__str__() + " already had permission: " + permission)

                permissions[permission] = True
            else:
                logger.warning("Value was neither True or False, ignoring setting permission: " + permission)

        return permissions

    async def set_users_groups(self, user_id: UUID, groups: [UUID]):
        sql = 'DELETE FROM users_groups WHERE "user" = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, user_id)

        sql = 'INSERT INTO users_groups ("user", "group") VALUES ($1, $2);'

        for group in groups:
            async with self.pool.acquire() as con:  # type: Connection
                await con.execute(sql, user_id, group)
