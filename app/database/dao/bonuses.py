from app.logger import logger

from datetime import datetime, timedelta
from uuid import uuid4, UUID
from typing import Union
from asyncpg import Connection, PostgresError
from asyncpg.pool import Pool
from app.models import Code, Bonus


class BonusesDao:
    def __init__(self, pool: Pool):
        self.pool = pool

    async def create_bonus(self, user_id: UUID, scan_id: UUID, points: int) -> Union[Bonus, None]:
        id = uuid4()
        created = datetime.utcnow()

        try:
            sql = 'INSERT INTO bonuses (id, user_claim, scan, created, points) VALUES ($1, $2, $3, $4, $5);'

            async with self.pool.acquire() as con:  # type: Connection
                await con.execute(sql, id, user_id, scan_id, created, points)
        except PostgresError as exc:
            logger.debug(exc)
            logger.error("Something went wrong when trying to create a bonus for scan: " + scan_id.__str__())
            return None

        bonus = Bonus()
        bonus.id = id
        bonus.user_claim_id = user_id
        bonus.scan_id = scan_id
        bonus.created = created
        bonus.points = points

        return bonus

    async def get_bonuses(self, amount: int = None):
        sql = 'SELECT id, user_claim, scan, points, created FROM bonuses ORDER BY created DESC'

        if amount is None:
            sql += ' LIMIT $1;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, amount)
        else:
            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, amount)

        bonuses = []
        for row in rows:
            bonus = Bonus()
            bonus.id = row["id"]
            bonus.user_claim_id = row["user_claim"]
            bonus.scan_id = row["scan"]
            bonus.points = row["points"]
            bonuses.append(bonus)

        return bonuses

    async def get_bonuses_by_user_id(self, user_id: UUID, amount: Union[int, None] = None) -> list:
        sql = 'SELECT id, scan, points, created FROM bonuses WHERE user_claim = $1 ORDER BY created DESC'

        if amount is not None:
            sql += ' LIMIT $2;'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, user_id, amount)
        else:
            sql += ';'

            async with self.pool.acquire() as con:  # type: Connection
                rows = await con.fetch(sql, user_id)

        bonuses = []
        for row in rows:
            bonus = Bonus()
            bonus.id = row["id"]
            bonus.user_claim_id = user_id
            bonus.scan_id = row["scan"]
            bonus.points = row["points"]
            bonus.created = row["created"]

            bonuses.append(bonus)

        return bonuses

    async def get_bonuses_count_by_user_id(self, user_id: UUID) -> int:
        sql = 'SELECT count(*) AS bonuses FROM bonuses WHERE user_claim = $1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, user_id)

        return row["bonuses"]

    async def get_time_from_latest_scan(self, code_id:UUID, user_id:UUID) -> timedelta:
        sql = 'SELECT created FROM bonuses WHERE "code" = $1 AND "user" = $2 ORDER BY created DESC LIMIT 1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, code_id, user_id)

        if row is None:
            return timedelta.max

        date_scanned = row["created"]

        return datetime.utcnow() - date_scanned

    async def get_bonus_by_scan_id(self, scan_id: UUID) -> Bonus:
        sql = 'SELECT id, "user", points, created, coordinates FROM bonuses WHERE "code" = $1 ORDER BY created ASC LIMIT 1;'

        async with self.pool.acquire() as con:  # type: Connection
            row = await con.fetchrow(sql, scan_id)

        if row is None:
            return None

        bonus = Bonus()
        bonus.id = row["id"]
        bonus.user_claim_id = row["user"]
        bonus.scan_id = scan_id
        bonus.points = row["points"]
        bonus.created = row["created"]

        return bonus

    async def update_points_for_bonus(self, bonus_id: UUID, points: int) -> int:
        sql = 'UPDATE bonuses SET points = $1 WHERE id = $2;'

        async with self.pool.acquire() as con:  # type: Connection
            await con.execute(sql, points, bonus_id)

        return points
