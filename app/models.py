from datetime import datetime, timedelta
from typing import NamedTuple, Union
from uuid import UUID


class User:
    id: UUID
    name: str
    email: str
    created: datetime


def user_to_json(user: User) -> dict:
    return {
        'user': {
            'id': user.id.__str__(),
            'name': user.name,
            'email': user.email,
            'created': user.created.isoformat(' '),
        }
    }


class Group:
    id: UUID
    name: str
    level: int
    created: datetime


def group_to_json(group: Group) -> dict:
    return {
        'group': {
            'id': group.id.__str__(),
            'name': group.name,
            'level': group.level,
            'created': group.created.isoformat(' ')
        }
    }


class Category:
    id: UUID
    name: str
    description: str
    info_url: str
    created: datetime


def category_to_json(category: Category) -> dict:
    return {
        'category': {
            'id': category.id.__str__(),
            'name': category.name,
            'description': category.description,
            'info_url': category.info_url,
            'created': category.created.isoformat(' ')
        }
    }


class Code:
    id: UUID
    url: str
    image: str
    owner: UUID
    created: datetime
    category: Category


def code_to_json(code: Code) -> dict:
    return {
        'group': {
            'id': code.id.__str__(),
            'url': code.url,
            'image': code.image,
            'created': code.created.isoformat(' '),
            'category': category_to_json(code.category)
        }
    }


class Scan:
    id: UUID
    user_scan_id: UUID
    code_id: UUID
    points: int
    created: datetime
    longitude: float
    latitude: float


def scan_to_json(scan: Scan) -> dict:
    if hasattr(scan, "longitude") and hasattr(scan, "latitude"):
        return {
            'scan': {
                'id': scan.id.__str__(),
                'user_scan_id': scan.user_scan_id.__str__(),
                'code_id': scan.code_id.__str__(),
                'points': scan.points.__str__(),
                'created': scan.created.isoformat(' '),
                'longitude': scan.longitude.__str__(),
                'latitude': scan.latitude.__str__()
            }
        }
    else:
        return {
            'scan': {
                'id': scan.id.__str__(),
                'user_scan_id': scan.user_scan_id.__str__(),
                'code_id': scan.code_id.__str__(),
                'points': scan.points.__str__(),
                'created': scan.created.isoformat(' ')
            }
        }


class Claim:
    id: UUID
    user_claim_id: UUID
    code_id: UUID
    points: int
    created: datetime
    longitude: float
    latitude: float


def claim_to_json(claim: Claim) -> dict:
    if hasattr(claim, "longitude") and hasattr(claim, "latitude"):
        return {
            'scan': {
                'id': claim.id.__str__(),
                'user_scan_id': claim.user_claim_id.__str__(),
                'code_id': claim.code_id.__str__(),
                'points': claim.points.__str__(),
                'created': claim.created.isoformat(' '),
                'longitude': claim.longitude.__str__(),
                'latitude': claim.latitude.__str__()
            }
        }
    else:
        return {
            'scan': {
                'id': claim.id.__str__(),
                'user_scan_id': claim.user_claim_id.__str__(),
                'code_id': claim.code_id.__str__(),
                'points': claim.points.__str__(),
                'created': claim.created.isoformat(' ')
            }
        }


class Bonus:
    id: UUID
    user_claim_id: UUID
    scan_id: UUID
    points: int
    created: datetime


class Rule:
    id: UUID
    name: str
    start_date: datetime
    end_date: datetime
    claim_points: int
    scan_points: list
    bonus_points: list
    non_user_points: int
    user_cooldown: timedelta
    non_user_cooldown: timedelta
    category_id: UUID
    code_id: UUID


class Session:
    id: UUID
    user: User
    hash: str
    created: datetime
    last_used: datetime
    last_ip: str


class PasswordCheckResult(NamedTuple):
    valid: bool
    user: Union[User, None]
