-- Database structure for Piratpartiet Piratjakten
-- SQL is written with PostgreSQL syntax

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users
(
    id                    UUID PRIMARY KEY,
    email                 TEXT UNIQUE NOT NULL,
    password              TEXT NOT NULL,
    password_force_change BOOLEAN NOT NULL DEFAULT FALSE,
    name                  TEXT NOT NULL,
    created               TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE TABLE groups
(
    id      UUID PRIMARY KEY,
    name    TEXT UNIQUE NOT NULL,
    level   INTEGER NOT NULL,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE TABLE permissions
(
    id   TEXT PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE categories
(
    id          UUID PRIMARY KEY,
    name        TEXT UNIQUE NOT NULL,
    description TEXT,
    info_url    TEXT NOT NULL,
    created     TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE TABLE codes
(
    id         UUID PRIMARY KEY,
    url        TEXT UNIQUE NOT NULL,
    image      TEXT NOT NULL,
    created    TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    "category" UUID NOT NULL REFERENCES categories(id)
);

CREATE TABLE scans
(
    id          UUID PRIMARY KEY,
    user_scan   UUID REFERENCES users(id),
    "code"      UUID NOT NULL REFERENCES codes(id),
    points      INTEGER NOT NULL,
    created     TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    coordinates POINT
);

CREATE TABLE claims
(
    id          UUID PRIMARY KEY,
    user_claim  UUID NOT NULL REFERENCES users(id),
    "code"      UUID UNIQUE NOT NULL REFERENCES codes(id),
    points      INTEGER NOT NULL,
    created     TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    coordinates POINT
);

CREATE TABLE bonuses
(
    id         UUID PRIMARY KEY,
    user_claim UUID NOT NULL REFERENCES users(id),
    scan       UUID NOT NULL REFERENCES scans(id),
    points     INTEGER NOT NULL,
    created    TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE TABLE rules
(
    id                UUID PRIMARY KEY,
    name              TEXT UNIQUE NOT NULL,
    start_date        TIMESTAMP WITHOUT TIME ZONE,
    end_date          TIMESTAMP WITHOUT TIME ZONE,
    claim_points      INTEGER NOT NULL,
    scan_points       INTEGER ARRAY NOT NULL,
    bonus_points      INTEGER ARRAY NOT NULL,
    non_user_points   INTEGER NOT NULL,
    user_cooldown     INTERVAL NOT NULL,
    non_user_cooldown INTERVAL NOT NULL,
    created           TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    "code"            UUID REFERENCES codes(id),
    "category"        UUID REFERENCES categories(id)
);

CREATE TABLE placements
(
    "user" UUID UNIQUE NOT NULL REFERENCES users(id),
    points INTEGER NOT NULL,
    level  INTEGER NOT NULL
);

CREATE INDEX idx_placements ON placements(points DESC);

CREATE TABLE email_verify_links
(
    "email" TEXT NOT NULL REFERENCES users(email),
    link    UUID UNIQUE NOT NULL,
    PRIMARY KEY ("email", link)
);

CREATE TABLE password_reset_links
(
    "email" TEXT UNIQUE NOT NULL REFERENCES users(email),
    link    UUID UNIQUE NOT NULL,
    PRIMARY KEY ("email", link)
);

CREATE TABLE verified_emails
(
    "email" TEXT UNIQUE NOT NULL REFERENCES users(email),
    PRIMARY KEY ("email")
);

CREATE TABLE verified_users
(
    "user" UUID UNIQUE NOT NULL REFERENCES users(id),
    PRIMARY KEY ("user")
);

CREATE TABLE groups_permissions
(
    "group"      UUID NOT NULL REFERENCES groups(id),
    "permission" TEXT NOT NULL REFERENCES permissions(id),
    PRIMARY KEY ("group", "permission")
);

CREATE TABLE users_groups
(
    "user"  UUID NOT NULL REFERENCES users(id),
    "group" UUID NOT NULL REFERENCES groups(id),
    PRIMARY KEY ("user", "group")
);

CREATE TABLE sessions
(
    id        UUID PRIMARY KEY,
    "user"    UUID REFERENCES users(id),
    hash      TEXT NOT NULL,
    created   TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    last_used TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    last_ip   TEXT NOT NULL
);

-- Create permissions
INSERT INTO permissions VALUES ('group_management',    'Skapa och redigera grupper');
INSERT INTO permissions VALUES ('delete_groups'  ,     'Ta bort grupper');
INSERT INTO permissions VALUES ('user_management',     'Skapa och redigera användare');
INSERT INTO permissions VALUES ('delete_users',        'Ta bort användare');
INSERT INTO permissions VALUES ('category_management', 'Skapa och redigera kategorier');
INSERT INTO permissions VALUES ('delete_categories',   'Ta bort kategorier');
INSERT INTO permissions VALUES ('verify',              'Verifiera partimedlemmar');

-- Create an administrator group
INSERT INTO groups VALUES ('00000000-0000-0000-0000-000000000000', 'Administratör', 100, localtimestamp);

-- Put all permissions in the administrator group
INSERT INTO groups_permissions VALUES ('00000000-0000-0000-0000-000000000000', 'group_management');
INSERT INTO groups_permissions VALUES ('00000000-0000-0000-0000-000000000000', 'delete_groups');
INSERT INTO groups_permissions VALUES ('00000000-0000-0000-0000-000000000000', 'user_management');
INSERT INTO groups_permissions VALUES ('00000000-0000-0000-0000-000000000000', 'delete_users');
INSERT INTO groups_permissions VALUES ('00000000-0000-0000-0000-000000000000', 'category_management');
INSERT INTO groups_permissions VALUES ('00000000-0000-0000-0000-000000000000', 'delete_categories');
INSERT INTO groups_permissions VALUES ('00000000-0000-0000-0000-000000000000', 'verify');

-- Create an administrator account with password "Admin1!" and then force a change upon first login
INSERT INTO users (id, email, password, password_force_change, name, created)
VALUES ('00000000-0000-0000-0000-000000000000', 'admin@piratpartiet.se', '$2b$12$M8ttq7/ftdJHjiD69IBCQeFgUQqhUPben/txmJu9up02J7WXtitmq', true, 'Systemadministratör', localtimestamp);

-- Verify the administrator e-mail
INSERT INTO verified_emails VALUES ('admin@piratpartiet.se');

-- Verify the administrator account
INSERT INTO verified_users VALUES ('00000000-0000-0000-0000-000000000000');

-- Create placement for admin account
INSERT INTO placements VALUES ('00000000-0000-0000-0000-000000000000', 0, 1);

-- Put the administrator user into the administrator group
INSERT INTO users_groups VALUES ('00000000-0000-0000-0000-000000000000', '00000000-0000-0000-0000-000000000000');

-- Create global default rule
INSERT INTO rules VALUES ('00000000-0000-0000-0000-000000000000', 'Standard', NULL, NULL, '15', '{10, 8, 6, 4, 2, 1}', '{15, 10, 5}', '5', '22:00:00', '00:30:00', localtimestamp, NULL, NULL);
