# Piratjakten
Aktivisthemsida för Piratpartiet.

## Sätta upp utvecklingsmiljön
### Front-end
Klientsidan körs med [Tornados inbyggda mallar](https://www.tornadoweb.org/en/stable/guide/templates.html) och [Semantic UI](https://semantic-ui.com/introduction/getting-started.html).

### Back-end
Installera Python 3.7.x **64-bit** (var noga eftersom länken till 64-bit är väl gömd) inklusive PIP.

Om du använder [PyCharm](https://www.jetbrains.com/pycharm/), skapa då en
[Virtualenv Environment](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)
för att slippa krångel med administratörsrättigheter. Öppna sedan `requirements.txt` och klicka på
knappen `Install requirements` för att installera paketen som behövs i projektet.

I övriga fall installeras paketen direkt via PIP med: `pip install -r requirements.txt`

En [PostgreSQL](https://www.postgresql.org/download/) databas behövs även, dokumentation för installation och körning
[finns här](https://www.postgresql.org/docs/11/index.html). Efter att man har skapat en ny databas så måste man sätta
upp rätt struktur i databasen. SQL-kommandonen i filen `db.sql` sätter upp den grundläggande strukturen.

När databasen är konfigurerad och redo, starta python-servern med `python app.py` eller igenom din IDE.
Servern kommer att generera en ny `config.ini` fil och där måste inloggningsuppgifterna för databasen skrivas in.

## Dokumentation
* [Teknisk beskrivning](https://docs.piratpartiet.se/products/files/doceditor.aspx?fileid=606&doc=Ti9zbW5yR3luV2hPN1VkK040cHBUVG5JWVI2eEFEOW8vOG8wMHpqNDJVST0_IjYwNiI1)
* [API](https://gitlab.com/piratpartiet-se/piratjakten/blob/develop/Piratjakten.json)
* Back-end: [Tornado](https://www.tornadoweb.org/en/stable/), [PostgreSQL](https://www.postgresql.org/about/)
* Front-end: [Semantic UI](https://semantic-ui.com/introduction/getting-started.html)
* [Gamification to improve our world](https://www.youtube.com/watch?v=v5Qjuegtiyc)
* [Användarguide för Piratjakten i EU-valet 2019](https://docs.piratpartiet.se/products/files/doceditor.aspx?fileid=2435&doc=Q1gwRXVJYTdiSmsvMWZxRUZLRGFaQTByOHViektDRDJXZEZDT1o2bFJtUT0_IjI0MzUi0)