$(document).ready(function() {
    progressBar = $('#progressLevel');
    var points = parseInt(progressBar.attr('data-value'));
    var nextLevel = parseInt(progressBar.attr('data-total'));

    while (points >= 100) {
        points -= 100;
        nextLevel -= 100;
    }

    $('#progressLevel').progress({
        percent: (points / nextLevel) * 100
    });
});