function getData(url = '') {
    return fetch(url, {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow",
        referrer: "same-origin"
    }).then(response => response.json());
}

$(document).ready(function () {
    var map = L.map('mapUser').setView([59.332, 18.064], 8);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    getData('/api/scans?amount=1000')
        .then(function(response) {
            var success = response["success"];
            var reason = response["reason"];

            if (success !== true) {
                console.log(reason);
                return;
            }

            var data = response["data"];

            for (var i = 0; i < data.length; i++) {
                var scan = data[i]["scan"];

                if (scan["longitude"] === undefined || scan["latitude"] === undefined) {
                    continue;
                }

                try {
                    L.marker([scan["latitude"], scan["longitude"]]).addTo(map)
                        .bindPopup(
                            'Scanning: ' + scan["created"] + '<br/>' +
                            'Poäng: ' + scan["points"]
                        );
                } catch(error) {
                    console.log(error)
                }
            }
        })
        .catch(error => console.error(error));

    getData('/api/claims?amount=1000')
        .then(function(response) {
            var success = response["success"];
            var reason = response["reason"];

            if (success !== true) {
                console.log(reason);
                return;
            }

            var data = response["data"];

            for (var i = 0; i < data.length; i++) {
                var claim = data[i]["scan"];

                if (claim["longitude"] === undefined || claim["latitude"] === undefined) {
                    continue;
                }

                try {
                    L.marker([claim["latitude"], claim["longitude"]]).addTo(map)
                        .bindPopup(
                            'Scanning: ' + claim["created"] + '<br/>' +
                            'Poäng: ' + claim["points"]
                        );
                } catch(error) {
                    console.log(error)
                }
            }
        })
        .catch(error => console.error(error));
});