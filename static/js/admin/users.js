$(document).ready(function () {
    $.fn.api.settings.successTest = function (response) {
        console.log(response);

        if (response && response['success'] != null) {
            return response['success'];
        }

        return false;
    };

    $('select[name="groups"]').dropdown();

    var userTable = $('#userTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "/api/users",
        createdRow: function (row, data, dataIndex) {
            $(row).data('uuid', data['id']);
            $(row).data('email', data['email']);
            $(row).data('verified', data['verified']);
        },
        columns: [
            { data: 'name' },
            { data: 'created' },
            {
                data: 'verified_email',
                render: function (data, type, row, meta) {
                    if (data === true) {
                        return '<i class="check icon"></i> ' + row['email'];
                    }

                    return '<i class="x icon"></i> Ej verifierad';
                }
            },
            {
                data: 'verified',
                render: function (data, type, row, meta) {
                    if (data === true) {
                        return '<i class="check icon"></i>';
                    }

                    return '<i class="x icon"></i>';
                }
            }
        ],
        language: {
            "search": "Sök: ",
            "loadingRecords": "Laddar...",
            "lengthMenu": "Visa _MENU_ användare per sida",
            "zeroRecords": "Hittade ingenting :(",
            "info": "Visar sida _PAGE_ av _PAGES_",
            "infoEmpty": "",
            "infoFiltered": "",
            "paginate": {
                "first": "Första",
                "last": "Sista",
                "next": "Nästa",
                "previous": "Föregående"
            },
            select: {
                rows: {
                    _: ""
                }
            }
        }
    });

    userTable.on('select', function (e, dt, type, indexes) {
        var rows = userTable.rows({ selected: true });
        var count = rows.count();
        var verifyCount = 0;

        $(rows.nodes()).each(function () {
            if ($(this).data('verified') === false) {
                verifyCount += 1;
            }
        });

        if (count > 0 && $('#editUsers').hasClass('disabled') === true) {
            $('#editUsers').removeClass('disabled');
        } else if (count < 1 && $('#editUsers').hasClass('disabled') === false){
            $('#editUsers').addClass('disabled');
        }

        $('#removeCount').text(count);
        $('#verifyCount').text(verifyCount);
    });
    userTable.on('deselect', function (e, dt, type, indexes) {
        var rows = userTable.rows({ selected: true });
        var count = rows.count();
        var verifyCount = 0;

        $(rows.nodes()).each(function () {
            if ($(this).data('verified') === false) {
                verifyCount += 1;
            }
        });

        if (count > 0 && $('#editUsers').hasClass('disabled') === true) {
            $('#editUsers').removeClass('disabled');
        } else if (count < 1 && $('#editUsers').hasClass('disabled') === false){
            $('#editUsers').addClass('disabled');
        }

        $('#removeCount').text(count);
        $('#verifyCount').text(verifyCount);
    });
    userTable.on('xhr', function (e, settings, json) {
        $('#removeCount').text(0);
        $('#verifyCount').text(0);

        if ($('#editUsers').hasClass('disabled') === false){
            $('#editUsers').addClass('disabled');
        }
    });

    var xsrf = $('input[name="_xsrf"]').val();

    $('#removeUsers').api({
        url: '/api/users',
        method: 'DELETE',
        data: {
            _xsrf: xsrf
        },
        beforeSend: function (settings) {
            settings.data.emails = [];
            var rows = userTable.rows({ selected: true });

            $(rows.nodes()).each(function () {
                settings.data.emails.push($(this).data('email'));
            });

            return settings;
        },
        onSuccess: function (response) {
            console.log(response['success']);

            $('#successMessage').html('<li class="list">Valda användare borttagna</li>');

            $('#errorMessage').hide();
            $('#successMessage').show();

            userTable.ajax.reload();
        },
        onFailure: function (response) {
            console.log(response['success']);

            $('#errorMessage').html('<li class="list">Misslyckades med att ta bort användarna</li>');

            $('#errorMessage').show();
            $('#successMessage').hide();
        }
    });

    $('#verifyUsers').api({
        url: '/api/users',
        method: 'PUT',
        data: {
            _xsrf: xsrf
        },
        beforeSend: function (settings) {
            settings.data.emails = [];
            settings.data.verify = [];
            var rows = userTable.rows({ selected: true });

            $(rows.nodes()).each(function () {
                if ($(this).data('verified') === false) {
                    settings.data.emails.push($(this).data('email'));
                    settings.data.verify.push(true);
                }
            });

            return settings;
        },
        onSuccess: function (response) {
            console.log(response['success']);

            $('#successMessage').html('<li class="list">Valda användare verifierade</li>');

            $('#errorMessage').hide();
            $('#successMessage').show();

            userTable.ajax.reload();
        },
        onFailure: function (response) {
            console.log(response['success']);

            $('#errorMessage').html('<li class="list">Misslyckades med att verifiera användarna</li>');

            $('#errorMessage').show();
            $('#successMessage').hide();
        }
    });

    var users_groups = new Object();

    $('#editUsers').click(function() {
        if ($('#removeCount').text() === '1') {
            var name = userTable.cell('.selected', 0);
            var row = userTable.row('.selected', 0).node();
            var email = $(row).data('email');
            var id = $(row).data('uuid');

            if (users_groups.hasOwnProperty(id) === false) {
                fetch('/api/user/' + id + '/groups', {
                    method: 'GET'
                }).then(res => res.json())
                .then(response => {
                    users_groups[id] = response.data;
                    $('select[name="groups"]').dropdown('set exactly', response.data);
                })
                .catch(error => console.error('Error:', error));
            } else {
                $('select[name="groups"]').dropdown('set exactly', users_groups[id]);
            }

            $('#editTitle').text("Redigerar " + name.data());
            $('input[name="username"]').val(name.data());
            $('input[name="email"]').val(email);

            $('input[name="username"]').attr('disabled', false);
            $('input[name="email"]').attr('disabled', false);

            $('input[name="username"]').attr('placeholder', "Användarnamn");
            $('input[name="email"]').attr('placeholder', "email@piratpartiet.se");

            $('#saveChanges').api({
                url: '/api/user/' + id,
                method: 'PUT',
                data: {
                    _xsrf: xsrf
                },
                beforeSend: function (settings) {
                    settings.data.name = $('input[name="username"]').val();
                    settings.data.email = $('input[name="email"]').val();

                    var groups = $('select[name="groups"]').dropdown('get value');
                    if (groups.length === 0) {
                        settings.data.groups = ["None"];
                    } else {
                        settings.data.groups = groups;
                    }

                    return settings;
                },
                onSuccess: function (response) {
                    console.log(response['success']);
                    users_groups[id] = $('select[name="groups"]').dropdown('get value');
                    $('#successMessage').html('<li class="list">Nya inställningar för användaren sparades</li>');

                    $('#errorMessage').hide();
                    $('#successMessage').show();
                },
                onFailure: function (response) {
                    console.log(response['success']);

                    switch (response['reason']) {
                        case "PERMISSION DENIED":
                            $('#errorMessage').html(
                                '<li class="list">Du har ej rättigheter till att hantera grupper</li>'
                            );
                            break;
                        default:
                            $('#errorMessage').html(
                                '<li class="list">Misslyckades med att spara inställningarna för användaren</li>'
                            );
                            break;
                    }

                    $('#errorMessage').show();
                    $('#successMessage').hide();
                }
            });
            $('.ui.modal').modal('show');
        } else if ($('#removeCount').text() !== '0') {
            var rows = userTable.rows({ selected: true }).nodes();
            $('select[name="groups"]').dropdown('set exactly', []);

            $(rows).each(function () {
                id = $(this).data('uuid');

                if (users_groups.hasOwnProperty(id) === false) {
                    fetch('/api/user/' + id + '/groups', {
                        method: 'GET'
                    }).then(res => res.json())
                    .then(response => {
                        users_groups[id] = response.data;
                        $('select[name="groups"]').dropdown('set selected', response.data);
                    })
                    .catch(error => console.error('Error:', error));
                } else {
                    $('select[name="groups"]').dropdown('set selected', users_groups[id]);
                }
            });

            $('#editTitle').text("Redigerar flera användare");
            $('input[name="username"]').val('');
            $('input[name="email"]').val('');

            $('input[name="username"]').attr('disabled', true);
            $('input[name="email"]').attr('disabled', true);

            $('input[name="username"]').attr('placeholder', "Olika värden");
            $('input[name="email"]').attr('placeholder', "Olika värden");

            $('#saveChanges').api({
                url: '/api/users',
                method: 'PUT',
                data: {
                    _xsrf: xsrf
                },
                beforeSend: function (settings) {
                    var groups = $('select[name="groups"]').dropdown('get value');
                    if (groups.length === 0) {
                        settings.data.groups = ["None"];
                    } else {
                        settings.data.groups = groups;
                    }

                    settings.data.emails = [];
                    var rows = userTable.rows({ selected: true });

                    $(rows.nodes()).each(function () {
                        settings.data.emails.push($(this).data('email'));
                    });

                    return settings;
                },
                onSuccess: function (response) {
                    console.log(response['success']);
                    users_groups = new Object();
                    $('#successMessage').html('<li class="list">Nya inställningar för användarna sparades</li>');

                    $('#errorMessage').hide();
                    $('#successMessage').show();

                    userTable.ajax.reload();
                },
                onFailure: function (response) {
                    console.log(response['success']);
                    $('#errorMessage').html('<li class="list">Misslyckades med att spara inställningarna för användarna</li>');

                    $('#errorMessage').show();
                    $('#successMessage').hide();
                }
            });
            $('.ui.modal').modal('show');
        }
    });
});