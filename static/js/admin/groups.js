function format(d) {
    var html = '';

    for (var id in d.permissions) {
        if (d.permissions[id]["value"] === true) {
            html += '<div id="' + id + '" class="ui toggle checkbox checked">' +
                        '<input type="checkbox" checked="checked">' +
                        '<label>' + d.permissions[id]["name"] + '</label>' +
                    '</div><br/><br/>';
        } else {
            html += '<div id="' + id + '" class="ui toggle checkbox">' +
                        '<input type="checkbox">' +
                        '<label>' + d.permissions[id]["name"] + '</label>' +
                    '</div><br/><br/>';
        }
    }

    html += '<button id="' + d.uuid + '" class="ui positive labeled icon button">Spara ändringar<i class="save icon"></i></button>';
    return html;
}

$(document).ready(function () {
    $.fn.api.settings.successTest = function (response) {
        console.log(response);

        if (response && response['success'] != null) {
            return response['success'];
        }

        return false;
    };

    var groupTable = $('#groupTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/api/groups",
        createdRow: function (row, data, dataIndex) {
            $(row).data('uuid', data['uuid']);
        },
        columns: [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": '',
                responsivePriority: 1
            },
            { data: 'name', responsivePriority: 2 },
            { data: 'size' },
            { data: 'created' },
            { data: 'level' }
        ],
        order: [[1, 'asc']],
        language: {
            "search": "Sök: ",
            "loadingRecords": "Laddar...",
            "lengthMenu": "Visa _MENU_ grupper per sida",
            "zeroRecords": "Hittade ingenting :(",
            "info": "Visar sida _PAGE_ av _PAGES_",
            "infoEmpty": "",
            "infoFiltered": "",
            "paginate": {
                "first": "Första",
                "last": "Sista",
                "next": "Nästa",
                "previous": "Föregående"
            },
            select: {
                rows: {
                    _: ""
                }
            }
        }
    });

    $('#groupTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = groupTable.row(tr);

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            var data = row.data();
            row.child(format(data)).show();
            tr.addClass('shown');

            $('#' + data.uuid).api({
                url: '/api/group/' + data.uuid,
                method: 'PUT',
                data: {
                    _xsrf: xsrf
                },
                beforeSend: function (settings) {
                    settings.data.permissions = [];
                    var checkboxes = row.child().children().children();

                    for (var i = 0; i < checkboxes.length; i++) {
                        var checkbox = checkboxes.eq(i);

                        if (checkbox.hasClass('checkbox') === true) {
                            input = checkbox.children().eq(0).prop("checked");
                            settings.data[checkbox.attr('id')] = input;
                        }
                    }

                    return settings;
                },
                onSuccess: function (response) {
                    console.log(response['success']);

                    $('#successMessage').html('<li class="list">Rättigheterna är uppdaterade</li>');

                    $('#errorMessage').hide();
                    $('#successMessage').show();
                },
                onFailure: function (response) {
                    console.log(response['success']);

                    $('#errorMessage').html('<li class="list">Misslyckades med att uppdatera rättigheterna för gruppen</li>');

                    $('#errorMessage').show();
                    $('#successMessage').hide();
                }
            });
        }
    });

    groupTable.on('select', function (e, dt, type, indexes) {
        var rows = groupTable.rows({ selected: true });
        var count = rows.count();

        if (count === 1 && $('#editGroup').hasClass('disabled') === true) {
            $('#editGroup').removeClass('disabled');
        } else if ($('#editGroup').hasClass('disabled') === false){
            $('#editGroup').addClass('disabled');
        }

        $('#removeGroupCount').text(count);
    });
    groupTable.on('deselect', function (e, dt, type, indexes) {
        var rows = groupTable.rows({ selected: true });
        var count = rows.count();

        if (count === 1 && $('#editGroup').hasClass('disabled') === true) {
            $('#editGroup').removeClass('disabled');
        } else if ($('#editGroup').hasClass('disabled') === false){
            $('#editGroup').addClass('disabled');
        }

        $('#removeGroupCount').text(count);
    });
    groupTable.on('xhr', function (e, settings, json) {
        $('#removeGroupCount').text(0);

        if ($('#editGroup').hasClass('disabled') === false){
            $('#editGroup').addClass('disabled');
        }
    });

    var xsrf = $('input[name="_xsrf"]').val();

    $('#removeGroups').api({
        url: '/api/groups',
        method: 'DELETE',
        data: {
            _xsrf: xsrf
        },
        beforeSend: function (settings) {
            settings.data.uuid = [];
            var rows = groupTable.rows({ selected: true });

            $(rows.nodes()).each(function () {
                settings.data.uuid.push($(this).data('uuid'));
            });

            return settings;
        },
        onSuccess: function (response) {
            console.log(response['success']);

            $('#successMessage').html('<li class="list">Valda grupper borttagna</li>');

            $('#errorMessage').hide();
            $('#successMessage').show();

            groupTable.ajax.reload();
        },
        onFailure: function (response) {
            console.log(response['success']);

            $('#errorMessage').html('<li class="list">Misslyckades med att ta bort valda grupper</li>');

            $('#errorMessage').show();
            $('#successMessage').hide();
        }
    });

    $('#editGroup').click(function() {
        var name = groupTable.cell('.selected', 1);
        var level = groupTable.cell('.selected', 4);
        var row = groupTable.row('.selected', 0).node();
        var id = $(row).data('uuid');

        $('#editTitle').text("Redigerar " + name.data());
        $('input[name="groupname"]').val(name.data());
        $('input[name="level"]').val(level.data());

        $('#saveChanges').api({
            url: '/api/group/' + id,
            method: 'PUT',
            data: {
                _xsrf: xsrf
            },
            beforeSend: function (settings) {
                settings.data.name = $('input[name="groupname"]').val();
                settings.data.level = $('input[name="level"]').val();

                return settings;
            },
            onSuccess: function (response) {
                console.log(response['success']);
                $('#successMessage').html('<li class="list">Nya inställningar för gruppen sparades</li>');

                $('#errorMessage').hide();
                $('#successMessage').show();

                groupTable.ajax.reload();
            },
            onFailure: function (response) {
                console.log(response['success']);
                $('#errorMessage').html('<li class="list">Misslyckades med att spara inställningarna för gruppen</li>');

                $('#errorMessage').show();
                $('#successMessage').hide();
            }
        });
        $('.ui.modal').modal('show');
    });
});