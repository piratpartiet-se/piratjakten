$(document).ready(function () {
    var xsrf = $('input[name="_xsrf"]').val();
    var category_id = $('input[name="_category_id"]').val();

    var codeTable = $('#codeTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "/api/codes/" + category_id,
        createdRow: function (row, data, dataIndex) {
            $(row).data('uuid', data['id']);
        },
        columns: [
            { data: 'id' },
            { data: 'created' },
            {
                data: 'image',
                render: function (data, type, row, meta) {
                    return '<img class="ui tiny image" src="' + "/images/qrcodes/" + data + '">'
                }
            }
        ],
        language: {
            "search": "Sök: ",
            "loadingRecords": "Laddar...",
            "lengthMenu": "Visa _MENU_ koder per sida",
            "zeroRecords": "Hittade ingenting :(",
            "info": "Visar sida _PAGE_ av _PAGES_",
            "infoEmpty": "",
            "infoFiltered": "",
            "paginate": {
                "first": "Första",
                "last": "Sista",
                "next": "Nästa",
                "previous": "Föregående"
            },
            select: {
                rows: {
                    _: ""
                }
            }
        }
    });

    codeTable.on('select', function (e, dt, type, indexes) {
        var rows = userTable.rows({ selected: true });
        var count = rows.count();
        var verifyCount = 0;

        $(rows.nodes()).each(function () {
            if ($(this).data('verified') === false)
                verifyCount += 1;
        });

        $('#removeCount').text(count);
        $('#markCount').text(verifyCount);
    });
    codeTable.on('deselect', function (e, dt, type, indexes) {
        var rows = userTable.rows({ selected: true });
        var count = rows.count();
        var verifyCount = 0;

        $(rows.nodes()).each(function () {
            if ($(this).data('verified') === false)
                verifyCount += 1;
        });

        $('#removeCount').text(count);
        $('#markCount').text(verifyCount);
    });

    $('#createCodes').api({
        url: '/api/code/' + category_id,
        method: 'POST',
        data: {
            _xsrf: xsrf
        },

        beforeSend: function (settings) {
            settings.data.no = $('#numberCodes').val();
            return settings;
        },
        onSuccess: function (response) {
            console.log(response);

            switch (response['reason']) {
                case "CREATED CODES":
                    $('#getCodes').attr('href', response['data']['image_url']);
                    break;
            }
        },
        onFailure: function (response) {
            console.log(response);
        }
    });
});
