$(document).ready(function () {
    $('.ui.accordion').accordion();
    $('.menu .item').tab();

    $.fn.api.settings.successTest = function (response) {
        console.log(response);

        if (response && response['success'] != null) {
            return response['success'];
        }

        return false;
    };

    /* Profil formulär */
    $('#profileForm').form({
        fields: {
            name: {
                identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Ange ditt namn' }]
            },
        }
    }).api({
        url: '/api/user/' + $('input[name="id"]').val(),
        serializeForm: true,
        method: 'PUT',
        onSuccess: function (response) {
            $('#errorEmail').hide();
            $('#successProfile').html('<ul class="list"><li>Nya profil inställningar sparade</li></ul>');
            $('#successProfile').show();
        },
        onFailure: function (response) {
            $('#successProfile').hide();

            switch (response['reason']) {
                case 'NAME MISSING':
                    $('#errorProfile').html('<ul class="list"><li>Namn saknades</li></ul>');
                    break;
                default:
                    $('#errorProfile').html('<ul class="list"><li>Okänt fel inträffade, försök igen senare</li></ul>');
                    break;
            }

            $('#errorProfile').show();
        }
    });

    /* Email formulär */
    $('#emailForm').form({
        fields: {
            password: {
                identifier: 'password',
                rules: [{ type: 'empty', prompt: 'För att spara ändringar så måste du ange ditt lösenord igen' }]
            },
            email: {
                identifier: 'email',
                rules: [{ type: 'email', prompt: 'Ange en giltig e-mail adress' }]
            },
        }
    }).api({
        url: '/api/user/' + $('input[name="id"]').val(),
        serializeForm: true,
        method: 'PUT',
        onSuccess: function (response) {
            $('input[name="password"]').val('');

            $('#errorEmail').hide();
            $('#successEmail').html('<ul class="list"><li>Ny e-mail adress sparad</li></ul>');
            $('#successEmail').show();
        },
        onFailure: function (response) {
            $('#successEmail').hide();

            switch (response['reason']) {
                case 'UPDATE NOT ALLOWED':
                    $('#errorEmail').html('<ul class="list"><li>Lösenordet var felaktigt</li></ul>');
                    break;
                case 'NEW E-MAIL IS ALREADY IN USE':
                    $('#errorEmail').html('<ul class="list"><li>E-mail adressen används redan av någon annan</li></ul>');
                    break;
                default:
                    $('#errorProfile').html('<ul class="list"><li>Okänt fel inträffade, försök igen senare</li></ul>');
                    break;
            }

            $('#errorEmail').show();
        }
    });

    /* Lösenords formulär */
    $('#passwordForm').form({
        fields: {
            password: {
                identifier: 'password',
                rules: [{ type: 'empty', prompt: 'För att spara ändringar så måste du ange ditt lösenord igen' }]
            },
            password1: {
                identifier: 'password1',
                rules: [{ type: 'empty', prompt: 'Nytt lösenord saknas' }]
            },
            password2: {
                identifier: 'password2',
                rules: [{ type: 'empty', prompt: 'Nytt lösenord saknas' }]
            },
        }
    }).api({
        url: '/api/user/' + $('input[name="id"]').val(),
        serializeForm: true,
        method: 'PUT',
        onSuccess: function (response) {
            $('input[name="password1"]').val('');
            $('input[name="password2"]').val('');
            $('input[name="password"]').val('');

            $('#errorPassword').hide();
            $('#successPassword').html('<ul class="list"><li>Nytt lösenord sparat</li></ul>');
            $('#successPassword').show();
        },
        onFailure: function (response) {
            $('#successPassword').hide();

            switch (response['reason']) {
                case 'UPDATE NOT ALLOWED':
                    $('#errorPassword').html('<ul class="list"><li>Lösenordet var felaktigt</li></ul>');
                    break;
                case 'PASSWORDS DO NOT MATCH':
                    $('#errorPassword').html('<ul class="list"><li>Nya lösenorden stämde inte överens</li></ul>');
                    break;
                default:
                    $('#errorPassword').html('<ul class="list"><li>Okänt fel inträffade, försök igen senare</li></ul>');
                    break;
            }

            $('#errorPassword').show();
        }
    });
});