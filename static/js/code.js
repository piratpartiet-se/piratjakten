$(document).ready(function () {
    var authenticated = $('input[name="_authenticated"]').val();
    var redirectURL = $('input[name="_redirectURL"]').val();
    var codeID = $('input[name="_codeID"]').val();
    var xsrf = $('input[name="_xsrf"]').val();

    if (authenticated === "False") {
        window.location.replace(redirectURL);
    } else if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(sendPosition, sendNoPosition);
    } else {
        $('.ui.active.dimmer').api({
            url: '/api/scan/' + codeID,
            on: 'now',
            method: 'POST',
            data: {
                _xsrf: xsrf,
                position: {
                    "longitude": position.coords.longitude,
                    "latitude": position.coords.latitude
                }
            },
            onSuccess: function (response) {
                window.location.replace("/");
            },
            onFailure: function (response) {
                console.log(response);
            }
        });
    }

    function sendPosition(position) {
        $('.ui.active.dimmer').api({
            url: '/api/scan/' + codeID,
            on: 'now',
            method: 'POST',
            data: {
                _xsrf: xsrf,
                position: {
                    "longitude": position.coords.longitude,
                    "latitude": position.coords.latitude
                }
            },
            onSuccess: function (response) {
                window.location.replace("/");
            },
            onFailure: function (response) {
                console.log(response);
            }
        });
    }

    function sendNoPosition(error) {
        $('.ui.active.dimmer').api({
            url: '/api/scan/' + codeID,
            on: 'now',
            method: 'POST',
            data: {
                _xsrf: xsrf,
                position: {
                    "longitude": "GEOLOCATION NOT SUPPORTED",
                    "latitude": "GEOLOCATION NOT SUPPORTED"
                }
            },
            onSuccess: function (response) {
                window.location.replace("/");
            },
            onFailure: function (response) {
                console.log(response);
            }
        });
    }
});